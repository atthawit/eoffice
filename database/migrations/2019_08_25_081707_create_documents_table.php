<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("document_number");
            $table->string("subject");
            $table->string("folder");
            $table->unsignedBigInteger("type_id");
            $table->unsignedBigInteger("priority_id");
            $table->unsignedBigInteger("secret_level_id");
            $table->string("attachment");
            $table->text("document_source");
            $table->text("document_destination");
            $table->unsignedBigInteger("purpose_id");
            $table->unsignedBigInteger("document_progress_id");
            $table->text("document_reject_reason");
            $table->unsignedBigInteger("document_create_by_user_id");
            $table->unsignedBigInteger("document_action_by_user_id");
            $table->text("keyword");
            $table->string("annotation");

            $table->timestamps();

            $table->foreign("document_create_by_user_id")->references("id")->on('users')->onDelete('cascade');
            $table->foreign("document_action_by_user_id")->references("id")->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
