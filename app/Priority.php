<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Priority extends Model
{
    protected $table = 'priorities';

    public function documents()
    {
        return $this->hasMany(Document::class);
    }
}
