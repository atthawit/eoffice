<?php

namespace App\Services;

use App\ThakamPassportAccount;
use App\User;
use Laravel\Socialite\Contracts\User as ProviderUser;

class ThakamPassportAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = ThakamPassportAccount::whereProvider('thakam')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {
            $account = new ThakamPassportAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'thakam'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'password' => md5(rand(1, 10000)),
                ]);
            }
            $account->user()->associate($user);
            $account->save();
            return $user;
        }
    }
}
