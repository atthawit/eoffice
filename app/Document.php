<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


class Document extends Model
{
    protected $table = 'documents';

    protected $casts = [
        'attachment' => 'array',
        'keyword' => 'array',
        'document_destination' => 'array'
    ];

    private function toThaiNumber($number)
    {
        $numthai = array("๑", "๒", "๓", "๔", "๕", "๖", "๗", "๘", "๙", "๐");
        $numarabic = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
        $str = str_replace($numarabic, $numthai, $number);
        return $str;
    }

    public function getDocumentNumberThaiAttribute()
    {
        return $this->toThaiNumber($this->document_number);
    }

    public function getDocumentRejectDateThaiAttribute()
    {
        return $this->toThaiNumber($this->document_reject_date);
    }

    public function getDocumentCreatedDateThaiAttribute()
    {
        return $this->toThaiNumber($this->document_created_date);
    }

    public function getDocumentAcceptDateThaiAttribute()
    {
        return $this->toThaiNumber($this->document_accept_date);
    }

    public function priority()
    {
        return $this->hasOne(Priority::class, 'id', 'priority_id');
    }

    public function progress()
    {
        return $this->hasOne(Progress::class, 'id', 'document_progress_id');
    }

    public function purpose()
    {
        return $this->hasOne(Purpose::class, 'id', 'purpose_id');
    }

    public function type()
    {
        return $this->hasOne(Type::class, 'id', 'type_id');
    }

    public function folder()
    {
        return $this->hasOne(Folder::class, 'id', 'folder_id');
    }

    public function secretLevel()
    {
        return $this->hasOne(SecreteLevel::class, 'id', 'secret_level_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'document_create_by_user_id', 'id');
    }

    public function userAction()
    {
        return $this->belongsTo(User::class, 'document_action_by_user_id', 'id');
    }

    public function scopeUnreceive($query)
    {
        return $query->where('document_progress_id', 1);
    }

    public function scopeUnsend($query)
    {
        return $query->where('document_progress_id', 2);
    }

    public function scopeReceive($query)
    {
        return $query->where('document_progress_id', 3);
    }

    public function scopeSend($query)
    {
        return $query->where('document_progress_id', 5);
    }

    public function scopeReject($query)
    {
        return $query->where('document_progress_id', 4);
    }

    public function scopeSendReject($query)
    {
        return $query->where('document_progress_id', 6);
    }

    public function scopeInbox($query)
    {
        return $query->where('folder_id', 1);
    }

    public function scopeOutbox($query)
    {
        return $query->where('folder_id', 2);
    }

    public function getSecretAttribute()
    {
        $allPermissions = [];
        $allSecretlevels = [
            '1' => 'view nomal document',
            '2' => 'view top secret document',
            '3' => 'view secret document',
            '4' => 'view confidential document',
            '5' => 'view conceal document'
        ];

        $documentSecretLevels = $allSecretlevels[$this->secret_level_id];

        if (Auth::check()) {
            $uid = Auth::user()->id;
        }

        $user = User::find($uid);


        foreach ($user->getAllPermissions() as $permissions) {
            array_push($allPermissions, $permissions->name);
        }

        if (in_array($documentSecretLevels, $allPermissions)) {
            return true;
        }
        return false;
    }


    // public function getUnreceiveCountAttribute()
    // {
    //     if ($this->unreceive()->count() > 0) {
    //         return $this->unreceive()->count();
    //     }
    // }

    // public function getUnsendCountAttribute()
    // {
    //     if ($this->unsend()->count() > 0) {
    //         return $this->unsend()->count();
    //     }
    // }

    public function getSecretClassAttribute()
    {
        switch ($this->secret_level_id) {
            case '1':
                return "label bg-green";
                break;
            case '5':
                return "label bg-blue";
                break;
            case '4':
                return "label bg-yellow";
                break;
            case '3':
                return "label bg-orange";
                break;
            case '2':
                return "label bg-red";
                break;
            default:
                return "";
        }
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    public function getReceivedNumberAttribute()
    {
        if (!empty($this->receive_number)) {
            $receiveNumberWithYear = $this->receive_number;
            $receiveNumber = explode("_", $receiveNumberWithYear);

            return $receiveNumber[0];
        }

        return null;
    }
}
