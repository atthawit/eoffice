<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentUnreceiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required',
            'subject' => 'required',
            'purpose' => 'required',
            'priority' => 'required',
            'secret_level' => 'required',
            'type' => 'required',
            'date' => 'required',
            'source' => 'required',
            'destination' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'number.required'    => 'จำเป็นต้องกรอกเลขที่หนังสือ',
            'subject.required'  => 'จำเป็นต้องกรอกเรื่องหนังสือ',
            'purpose.required'  => 'จำเป็นต้องเลือกการปฏิบัติ',
            'priority.required'  => 'จำเป็นต้องเลือกชั้นความสำคัญ',
            'secret_level.required'  => 'จำเป็นต้องเลือกชั้นความลับ',
            'type.required'  => 'จำเป็นต้องเลือกประเภทหนังสือ',
            'date.required'  => 'จำเป็นต้องเลือกวันที่ลงในหนังสือ',
            'source.required'  => 'จำเป็นต้องเลือกหน่วยงานต้นทาง',
            'destination.required'  => 'จำเป็นต้องเลือกหน่วยงานปลายทาง'
        ];
    }
}
