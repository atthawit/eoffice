<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
use App\Folder;
use App\Priority;
use App\Progress;
use App\Purpose;
use App\SecreteLevel;
use App\Type;

class SearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $folders = Folder::all();
        $priorities = Priority::all();
        $progresses = Progress::all();
        $purposes = Purpose::all();
        $types = Type::all();
        $secretLevels = SecreteLevel::all();

        return view('search.index', ['folders' => $folders, 'priorities' => $priorities, 'progresses' => $progresses, 'purposes' => $purposes, 'types' => $types, 'secretLevels' => $secretLevels,]);
    }

    public function search(Request $request, Document $document)
    {
        $document = $document->newQuery();

        if ($request->receive_number) {

            $receiveNumberStr = str_replace(' ', '%', $request->receive_number);

            $document->where('receive_number', 'like', '%' . $receiveNumberStr . '%');
        }

        if ($request->tag) {

            $receiveTagStr = str_replace(' ', '%', $request->tag);

            $document->whereJsonContains('keyword', $receiveTagStr);
        }

        if ($request->number) {

            $numberStr = str_replace(' ', '%', $request->number);

            $document->where('document_number', 'like', '%' . $numberStr . '%');
        }

        if ($request->subject) {

            $subjectStr = str_replace(' ', '%', $request->subject);

            $document->where('subject', 'like', '%' . $subjectStr . '%');
        }


        if ($request->priority) {
            $document->where('priority_id', 'like', $request->priority);
        }

        if ($request->secret_level) {
            $document->where('secret_level_id', 'like', $request->secret_level);
        }

        if ($request->type) {
            $document->where('type_id', 'like', $request->type);
        }

        if ($request->date_start && $request->date_end) {
            // $document->where('document_created_date', 'like', $request->date);



            $document->whereBetween('document_created_date', [$request->date_start, $request->date_end]);
        }

        if ($request->source) {

            $sourceStr = str_replace(' ', '%', $request->source);

            $document->where('document_source', 'like', '%' . $sourceStr . '%');
        }

        if ($request->destination) {

            $destinationStr = str_replace(' ', '%', $request->destination);

            $document->where('document_destination', 'like', '%' . $destinationStr . '%');
        }

        if ($request->purpose) {
            $document->where('purpose_id', 'like', $request->purpose);
        }

        if ($request->keyword) {

            $keywordStr = str_replace(' ', '%', $request->keyword);

            // $document->where('keyword', 'like', '%' . $keywordStr . '%');
            $document->whereJsonContains('keyword', $keywordStr);
        }

        // dd($document);
        if (!$request->folder) {
            $request->folder = [null];
        }

        if ((count($request->folder) == 2) && ($request->folder[0] == 'inboxchecked') && ($request->folder[1] == 'outboxchecked')) {
            return view('search/result', ['documents' => $document->paginate(10)]);
        } else if ($request->folder[0] == 'inboxchecked') {
            // return $document->inbox()->get();
            return view('search/result', ['documents' => $document->inbox()->paginate(10)]);
        } else if ($request->folder[0] == 'outboxchecked') {
            // return $document->outbox()->get();
            return view('search/result', ['documents' => $document->outbox()->paginate(10)]);
        } else {
            // return $document->get();
            return view('search/result', ['documents' => $document->paginate(10)]);
        }

        return view('search/result', ['documents' => $document->paginate(10)]);
    }
}
