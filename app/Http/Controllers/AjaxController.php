<?php

namespace App\Http\Controllers;

use App\Attachment;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function deleteAttachment(Request $request)
    {
        if ($request->ajax()) {
            $id = $request->ca_id;

            $attachment = Attachment::find($id);

            if (!empty($attachment)) {
                $attachment->delete();
                return response()->json(['success' => 'success']);
            }
            return response()->json(['error' => 'error']);
        }
    }
}
