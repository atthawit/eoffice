<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectWithRoleController extends Controller
{
    public function __invoke()
    {
        $homeMenus = [
            ["permission" => "view inbox unreceive menu", "link" => "unreceive"],
            ["permission" => "view inbox receive menu", "link" => "receive"],
            ["permission" => "view outbox unsend menu", "link" => "unsend"],
            ["permission" => "view outbox send menu", "link" => "send"],
            ["permission" => "view inbox reject menu", "link" => "reject"],
            ["permission" => "view outbox reject-send menu", "link" => "reject-send"],
            ["permission" => "view search menu", "link" => "search"]
        ];

        if (Auth::check()) {
            $redirectLink = $this->searchArray($homeMenus);
        }


        if (!empty($redirectLink)) {
            $link = '/documents/' . $redirectLink;
            return redirect($link);
        }
        return view('user_denied');
    }

    private function searchArray($homeMenus)
    {
        $permissions = [];

        foreach ((array) Auth()->user()->getAllPermissions()->pluck('name') as $p) {
            array_push($permissions, $p);
        }

        foreach ($homeMenus as $homeMenu) {
            if (in_array($homeMenu["permission"], $permissions[0])) {
                return $homeMenu["link"];
            }
        }
        return null;
    }
}
