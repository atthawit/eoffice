<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\Services\ThakamPassportAccountService;

class ThakamPassportAuthController extends Controller
{
    public function redirect()
    {
        return Socialite::driver('thakam')->redirect();
    }

    public function callback(ThakamPassportAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::driver('thakam')->user());
        auth()->login($user);
        return redirect()->route('home_redirect');
    }
}
