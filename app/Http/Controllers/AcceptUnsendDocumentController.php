<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
use App\Notifications\SendDocument;
use App\User;
use Spatie\Permission\Models\Role;


class AcceptUnsendDocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('permission:accept document');
    }
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $document = Document::findOrFail($request->unsend);

        $document->document_progress_id = 5;
        $document->document_action_by_user_id = Auth()->id();
        $document->document_accept_date = date("d/m/Y");


        $document->save();

        $this->sentNotify($document->id);

        return back();
    }

    public function sentNotify($id)
    {
        $document = Document::findOrFail($id);
        $roles = Role::all();
        $roleName = [];

        foreach ($roles as $role) {
            array_push($roleName, $role->name);
        }

        if (in_array('ทั้งหมด', $document->document_destination)) {
            foreach (User::all() as $user) {
                $user->notify(new SendDocument($id, $document->subject, $document->folder_id, $document->document_progress_id));
            }

            return;
        }

        if (!empty($document)) {
            foreach ($document->document_destination as $destination) {
                if (in_array($destination, $roleName)) {
                    foreach (User::role($destination)->get() as $user) {
                        $user->notify(new SendDocument($id, $document->subject, $document->folder_id, $document->document_progress_id));
                    }
                }
            }

            return;
        }
    }
}
