<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;

class TypeAheadSearchController extends Controller
{
    public function search(Request $request)
    {
        $pid = $request->pid;

        if ($pid == 1) {
            $results = Document::where('subject', 'like', '%' . $request->keyword . '%')->unreceive()->get();
        } else if ($pid == 2) {
            $results = Document::where('subject', 'like', '%' . $request->keyword . '%')->unsend()->get();
        } else if ($pid == 3) {
            $results = Document::where('subject', 'like', '%' . $request->keyword . '%')->receive()->get();
        } else if ($pid == 4) {
            $results = Document::where('subject', 'like', '%' . $request->keyword . '%')->reject()->get();
        } else if ($pid == 5) {
            $results = Document::where('subject', 'like', '%' . $request->keyword . '%')->send()->get();
        } else if ($pid == 6) {
            $results = Document::where('subject', 'like', '%' . $request->keyword . '%')->sendReject()->get();
        } else {
            $results = Document::where('subject', 'like', '%' . $request->keyword . '%')->get();
        }

        $formatResults = [];

        if ($results->count() > 0) {
            foreach ($results as $result) {
                array_push($formatResults, ['id' => $result['id'], 'subject' => $result['subject'], 'document_progress_id' => $result['document_progress_id']]);
            }
        }

        return response($formatResults)->header('Content-Type', 'application/json');;

        // return response($formatResults)->header('Content-Type', 'application/json');;
    }
}
