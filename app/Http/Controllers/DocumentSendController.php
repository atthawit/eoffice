<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
use App\Folder;
use App\Priority;
use App\Progress;
use App\Purpose;
use App\SecreteLevel;
use App\Type;
use Spatie\Permission\Models\Role;
use App\Http\Requests\DocumentRequest;

class DocumentSendController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documents = Document::with(['priority', 'secretLevel'])->send()->latest()->paginate(15);

        return view('outbox.send.index', ['documents' => $documents]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document = Document::findOrFail($id);
        $this->authorize('show', $document);

        return view('outbox.send.show', ['document' => $document]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $folders = Folder::all();
        $priorities = Priority::all();
        $progresses = Progress::all();
        $purposes = Purpose::all();
        $types = Type::all();
        $secretLevels = SecreteLevel::all();
        $roles = Role::all();

        $document = Document::findOrFail($id);
        $this->authorize('edit', $document);

        return view('outbox.send.edit', ['document' => $document, 'folders' => $folders, 'priorities' => $priorities, 'progresses' => $progresses, 'purposes' => $purposes, 'types' => $types, 'secretLevels' => $secretLevels, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DocumentRequest $request, $id)
    {
        $document = Document::findOrFail($id);
        $this->authorize('update', $document);

        // Document number
        $document->document_number = $request->number;

        // Document subject
        $document->subject = $request->subject;

        // Document purpose
        $document->purpose_id = $request->purpose;

        // Document folder, 2 means outbox folder
        $document->folder_id = 4;

        // Document progress, 5 means sent document
        $document->document_progress_id = 5;

        // Document priority
        $document->priority_id = $request->priority;

        // Document secret level
        $document->secret_level_id = $request->secret_level;

        // Document type
        $document->type_id = $request->type;

        // // Document attachment
        // $document->attachment = $request->attachment;

        // Document create date
        $document->document_created_date = $request->date;

        // Document source
        $document->document_source = $request->source;

        // Document destination
        $document->document_destination = $request->destination;

        // Document keyword
        $document->keyword = $request->keyword;

        // Document annotaion
        $document->annotation = $request->annotation;

        // User who created document
        $document->document_create_by_user_id = Auth()->id();

        // Attachment upload

        // if ($request->hasfile('attachment')) {
        //     foreach ($request->file('attachment') as $attachment) {
        //         $name = (new \DateTime())->format('His') . "_" . $attachment->getClientOriginalName();
        //         $attachment->move(public_path() . '/uploads/documents/', $name);
        //         $data[] = ["original_name" => $attachment->getClientOriginalName(), "store_name" => $name];
        //     }
        //     $document->attachment = $data;
        // }

        $document->save();

        if ($request->hasfile('attachment')) {
            foreach ($request->file('attachment') as $attachment) {
                $name = (new \DateTime())->format('His') . "_" . $attachment->getClientOriginalName();
                $attachment->move(public_path() . '/uploads/documents/', $name);
                // $data[] = ["original_name" => $attachment->getClientOriginalName(), "store_name" => $name];
                $document->attachments()->create([
                    'original_name' => $attachment->getClientOriginalName(),
                    'store_name' => $name
                ]);
            }
            // $document->attachment = $data;
        }

        return redirect()->route('send.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $document = Document::find($id);
        $this->authorize('destroy', $document);
        $document->delete();

        return redirect()->route('send.index')->with('success', "Your document has been deleted.");
    }
}
