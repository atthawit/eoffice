<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserManageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(20);

        return view('system_manage.user.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        $roles = Role::all();
        $permissions = Permission::all();

        return view('system_manage.user.create', ['user' => $user, 'roles' => $roles, 'allpermissions' => $permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->hasfile('avatar')) {
            $avatar = $request->file('avatar');
            $name = (new \DateTime())->format('His') . "_" . $avatar->getClientOriginalName();
            $avatar->move(public_path() . '/uploads/avatar/', $name);
            $user->avatar = $name;
        }

        if ($request->filled('password')) {
            $user->password = bcrypt($request->password);
        }

        foreach ($request->role as $role) {
            $user->assignRole($role);
        }

        foreach ($request->user_define_permission as $user_define_permission) {
            $user->givePermissionTo($user_define_permission);
        }

        $user->save();

        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        $permissions = Permission::all();

        return view('system_manage.user.edit', ['user' => $user, 'roles' => $roles, 'allpermissions' => $permissions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->hasfile('avatar')) {
            $avatar = $request->file('avatar');
            $name = (new \DateTime())->format('His') . "_" . $avatar->getClientOriginalName();
            $avatar->move(public_path() . '/uploads/avatar/', $name);
            $user->avatar = $name;
        }

        if ($request->filled('password')) {
            $user->password = bcrypt($request->password);
        }

        $user->syncRoles($request->role);

        $user->syncPermissions($request->user_define_permission);


        $user->save();

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        return back();
    }
}
