<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class ValidationHelperController extends Controller
{
    public function isValidReceiveNumber(Request $request)
    {
        $messages = [
            'unique' => 'ฟิลด์เลขรับหนังสือต้องมีค่าไม่ซ้ำกันในฐานข้อมูล',
        ];

        $validator = Validator::make($request->all(), [
            'receive_number_with_year' => 'unique:documents,receive_number',
        ], $messages);

        if ($validator->passes()) {
            return response()->json(['success' => 'สามารถใช้เลขรับหนังสือนี้ได้']);
        }

        return response()->json(['error' => $validator->errors()->all()]);
    }
}
