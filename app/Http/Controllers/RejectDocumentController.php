<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;

class RejectDocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $id)
    {
        $document = Document::findOrFail($id);

        $document->document_progress_id = 4;
        $document->document_action_by_user_id = Auth()->id();
        $document->document_reject_reason = $request->reason;
        $document->document_reject_date = date("d/m/Y");

        $document->save();

        return back();
    }
}
