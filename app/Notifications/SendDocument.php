<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendDocument extends Notification
{
    use Queueable;

    private $documentId;
    private $subject;
    private $folder;
    private $progress;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($did, $subject, $folder, $progress)
    {
        $this->documentId = $did;
        $this->subject = $subject;
        $this->folder = $folder;
        $this->progress = $progress;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {
        return [
            'did' => $this->documentId,
            'folder' => $this->folder,
            'subject' => $this->subject,
            'progress' => $this->progress
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
