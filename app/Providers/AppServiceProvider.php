<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\ThakamPassport\ThakamProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        return $this->bootThakamPassportSocialite();
    }

    private function bootThakamPassportSocialite()
    {
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');
        $socialite->extend(
            'thakam',
            function ($app) use ($socialite) {
                $config = $app['config']['services.thakam'];
                return $socialite->buildProvider(ThakamProvider::class, $config);
            }
        );
    }
}
