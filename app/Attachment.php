<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable = [
        'original_name',
        'store_name',
    ];

    public function document()
    {
        return $this->belongsTo(Document::class);
    }
}
