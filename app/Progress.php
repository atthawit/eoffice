<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Progress extends Model
{
    protected $table = 'progresses';

    public function documents()
    {
        return $this->hasMany(Document::class);
    }
}
