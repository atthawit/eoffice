<?php

namespace App\Policies;

use App\Document;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DocumentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     //
    // }
    public $mapSecretLevelToPermission = [
        '1' => 'view nomal document',
        '2' => 'view top secret document',
        '3' => 'view secret document',
        '4' => 'view confidential document',
        '5' => 'view conceal document'
    ];

    public function create(User $user, Document $document)
    {
        return $user->hasPermissionTo('create document');
    }

    public function show(User $user, Document $document)
    {
        return $user->hasAnyPermission($this->mapSecretLevelToPermission[$document->secret_level_id]);
    }

    public function edit(User $user, Document $document)
    {
        return $user->hasPermissionTo('edit document');
    }

    public function update(User $user, Document $document)
    {
        return $user->hasPermissionTo('update document');
    }

    public function store(User $user, Document $document)
    {
        return $user->hasPermissionTo('update document');
    }

    public function destroy(User $user, Document $document)
    {
        return $user->hasPermissionTo('delete document');
    }
}
