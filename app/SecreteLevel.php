<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecreteLevel extends Model
{
    protected $table = 'secrete_levels';

    public function documents()
    {
        return $this->hasMany(Document::class);
    }
}
