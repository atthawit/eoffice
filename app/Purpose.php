<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purpose extends Model
{
    protected $table = 'purposes';

    public function documents()
    {
        return $this->hasMany(Document::class);
    }
}
