<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use App\User;

Route::get('/', function () {
    if (Auth::check()) {
        return redirect()->route('home_redirect');
    }
    return redirect()->route('login');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/documents/unreceive', 'DocumentUnreceiveController')->middleware('can:view inbox unreceive menu');
Route::post('documents/unreceive/{unreceive}/reject', 'RejectDocumentController')->name('unreceive.reject');
Route::post('documents/unreceive/{unreceive}/accept', 'AcceptDocumentController')->name('unreceive.accept');

Route::resource('/documents/receive', 'DocumentReceiveController')->middleware('can:view inbox receive menu');
Route::resource('/documents/reject', 'DocumentRejectController')->middleware('can:view inbox reject menu');

Route::resource('/documents/unsend', 'DocumentUnsendController')->middleware('can:view outbox unsend menu');
Route::post('documents/unsend/{unsend}/reject', 'RejectUnsendDocumentController')->name('unsend.reject');
Route::post('documents/unsend/{unsend}/accept', 'AcceptUnsendDocumentController')->name('unsend.accept');

Route::post('documents/helper/validate_receive_number', 'ValidationHelperController@isValidReceiveNumber')->name('helper.validate_receive_number');

Route::resource('/documents/send', 'DocumentSendController')->middleware('can:view outbox send menu');

Route::resource('/documents/reject-send', 'DocumentSendRejectController')->middleware('can:view outbox reject-send menu');

Route::get('/documents/search', 'SearchController@index')->name('search')->middleware('can:view search menu');
Route::get('/documents/search/result', 'SearchController@search')->name('result');

Route::get('/typeahead-search/{pid}', 'TypeAheadSearchController@search');



Route::get('/modal', function () {
    return view('layouts.shared.modals.modal');
});


Route::get('/notify', function () {
    // $user = Auth::user();

    // $user->notify(new SendDocument(User::findOrFail(3)));
    \Notification::send(User::all(), new SendDocument());

    // foreach (Auth::user()->notifications as $notification) {
    //     dd($notification);
    // }
});

Route::get('/sentnotify/{id}', 'AcceptUnsendDocumentController@sentNotify');

Route::get('/read/{id}', function ($id) {
    auth()->user()->unreadNotifications->find($id)->markAsRead();

    $notification = auth()->user()->unreadNotifications->find($id);

    return back();
})->name('notification.read');

Route::resource('/system_manage/user', 'UserManageController')->middleware('can:view user manage menu');
Route::resource('/system_manage/role', 'RoleManageController')->middleware('can:view role menu');
Route::resource('/profile', 'ProfileController');

Route::post('/ajax/delete-attachment', 'AjaxController@deleteAttachment')->name('ajax.delete_attachment');

Route::get('/documents', 'RedirectWithRoleController')->name('home_redirect');

Route::get('/documents/tag/{tag}', 'SearchController@search')->name('tag');

Route::get('/thakam-passport/redirect', 'ThakamPassportAuthController@redirect');
Route::get('/thakam-passport/callback', 'ThakamPassportAuthController@callback');

Route::view('/user-denied', 'user_denied');
