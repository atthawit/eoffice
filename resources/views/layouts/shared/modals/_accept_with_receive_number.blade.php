<div class="modal fade" id="modal-accept">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="accept-form" action="" method="POST">
                    @csrf
                    <p class="text-center" id="accept-modal-number-content"></p>
                    <div class="row">
                        <div class="col-md-3">
                            <label for="receive_number">เลขรับหนังสือ</label>
                        </div>
                        <div class="col-md-9">
                            <input type="text" name="receive_number" id="receive_number" placeholder="เลขรับหนังสือ"
                                class="form-control">
                            <input type="hidden" id="receive_number_with_year" name="receive_number_with_year">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <br />
                            <div class="alert hide" role="alert" id="alert_validate">
                                ทดสอบ
                            </div>
                        </div>
                    </div>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">ปิด</button>
                <button type="button" id="accept-btn" class="btn btn-primary"
                    onclick="event.preventDefault(); document.getElementById('accept-form').submit();"></button>
            </div>
        </div>
    </div>
</div>