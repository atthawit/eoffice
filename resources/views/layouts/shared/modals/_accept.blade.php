<div class="modal fade" id="modal-accept">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="accept-form" action="" method="POST">
                    @csrf
                    <p class="text-center" id="accept-modal-number-content"></p>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">ปิด</button>
                <button type="button" id="accept-btn" class="btn btn-primary"
                    onclick="event.preventDefault(); document.getElementById('accept-form').submit();"></button>
            </div>
        </div>
    </div>
</div>