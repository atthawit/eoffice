<div class="modal fade" id="modal-edit-user">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="edit-form" action="" method="POST">
                    @csrf
                    <div>
                        <ul class="nav nav-tabs" id="userTabs" role="tablist">
                            <li role="presentation" class="active"><a href="#user-data" id="user-data-tab" role="tab"
                                    data-toggle="tab" aria-controls="user-data"
                                    aria-expanded="true">แก้ไขข้อมูลส่วนตัว</a>
                            </li>
                            <li role="presentation" class=""><a href="#user-role" role="tab" id="user-role-tab"
                                    data-toggle="tab" aria-controls="user-role"
                                    aria-expanded="false">จัดการบทบาทผู้ใช้</a></li>
                            <li role="presentation" class=""><a href="#user-permission" role="tab"
                                    id="user-permission-tab" data-toggle="tab" aria-controls="user-permission"
                                    aria-expanded="false">จัดการสิทธิ์ของผู้ใช้</a></li>
                        </ul>
                        <div class="tab-content" id="userTabContent">
                            <div class="tab-pane fade active in" role="tabpanel" id="user-data"
                                aria-labelledby="user-data-tab">
                                <br />
                                <img src="http://localhost:8000/uploads/avatar/user-atthawit.jpg"
                                    class="img-circle img-responsive center-block" alt="User Image">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="avatar">รูปประจำตัว</label>
                                        <input type="file" class="form-control" id="avatar">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">ชื่อ</label>
                                        <input type="text" class="form-control" id="name" placeholder="ชื่อ">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">อีเมล</label>
                                        <input type="email" class="form-control" id="email" placeholder="อีเมล">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">รหัสผ่าน</label>
                                        <input type="password" class="form-control" id="password"
                                            placeholder="รหัสผ่าน">
                                    </div>
                                    <div class="form-group">
                                        <label for="verify_password">ยืนยันรหัสผ่าน</label>
                                        <input type="password" class="form-control" id="verify_password"
                                            placeholder="ยืนยันรหัสผ่าน">
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" role="tabpanel" id="user-role" aria-labelledby="user-role-tab">
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="role">บทบาท</label>
                                        <select class="form-control select2" multiple="multiple" id="role" name="role[]"
                                            style="width: 100%;" placeholder="บทบาท">

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" role="tabpanel" id="user-permission"
                                aria-labelledby="user-permission-tab">
                                <div class="box-body">
                                    <strong>สิทธิ์ของผู้ใช้ปัจจุบันตามบทบาท</strong>
                                    <hr />

                                </div>
                            </div>


                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">ปิด</button>
                <button type="button" id="edit-btn" class="btn btn-primary"
                    onclick="event.preventDefault(); document.getElementById('edit-form').submit();">บันทึก</button>
            </div>
        </div>
    </div>
</div>