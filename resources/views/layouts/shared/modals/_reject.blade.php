<div class="modal modal-warning fade" id="modal-reject">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>

            <div class="modal-body">
                <form id="reject-form" action="" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <p>หน่วยงานต้นทาง</p>
                            <p>เลขที่หนังสือ</p>
                            <p>เหตุผลการตีกลับ</p>
                        </div>
                        <div class="col-md-6">
                            <p id="reject-modal-source-content"></p>
                            <p id="reject-modal-number-content"></p>
                            <div class="form-group">
                                <textarea class="form-control" name="reason" rows="3"
                                    placeholder="เหตุผลการตีกลับ"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">ปิด</button>
                <button type="button" class="btn btn-outline"
                    onclick="event.preventDefault(); document.getElementById('reject-form').submit();">ตีกลับหนังสือ</button>
            </div>
        </div>
    </div>
</div>