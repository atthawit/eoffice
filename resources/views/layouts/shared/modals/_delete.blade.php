<div class="modal modal-danger fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p class="text-center" id="delete-modal-number-content"></p>
            </div>
            <form id="delete-form" action="" method="POST" style="display:none">
                @method('DELETE')
                @csrf
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">ปิด</button>
                <button type="button" class="btn btn-outline"
                    onclick="event.preventDefault(); document.getElementById('delete-form').submit();">ลบข้อมูล</button>
            </div>

        </div>
    </div>
</div>