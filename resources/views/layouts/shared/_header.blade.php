<header class="main-header">
  <!-- Logo -->
  <a href="/" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>E</b>OC</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>E - Office</b></span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="dropdown messages-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-envelope-o"></i>
            <span class="label label-success">{{ count(auth()->user()->unreadNotifications) }}</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">คุณมีเอกสาร {{ count(auth()->user()->unreadNotifications) }} ฉบับ</li>
            <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu">
                @foreach (auth()->user()->unreadNotifications as $notification)
                <li>
                  <a href="{{ route('notification.read', $notification->id) }}">
                    <div class="pull-left">
                      @if ($notification->data['folder'] == '1')
                      <i class="fa fa-download"></i>
                      @elseif($notification->data['folder'] == '2')
                      <i class="fa fa-upload"></i>
                      @endif

                    </div>
                    <h4>
                      {{ str_limit($notification->data['subject'], 25) }}
                      {{-- <small><i class="fa fa-clock-o"></i> 5 mins</small> --}}
                    </h4>
                    @php
                    $progressMatch = [
                    "1" => "unreceive",
                    "2" => "unsend",
                    "3" => "receive",
                    "4" => "reject",
                    "5" => "send",
                    "6" => "reject-send"
                    ]
                    @endphp
                    <p><span
                        class="pull-left">{{ \Carbon\Carbon::parse($notification->created_at)->diffForHumans() }}</span>
                      <a class="pull-right"
                        href="/documents/{{$progressMatch[$notification->data['progress']]}}/{{ $notification->data['did'] }}"
                        target="_blank"><strong>อ่าน</strong></a>
                    </p>
                  </a>
                </li>
                @endforeach


              </ul>
            </li>
            {{-- <li class="footer"><a href="#">See All Messages</a></li> --}}
          </ul>
        </li>
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ asset('/uploads/avatar') }}/{{Auth()->user()->avatar ?? 'user-icon.png'}}" class="user-image"
              alt="User Image">
            <span class="hidden-xs">{{ Auth::user()->name }}</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="{{ asset('/uploads/avatar') }}/{{Auth()->user()->avatar ?? 'user-icon.png'}}" class="img-circle"
                alt="User Image">

              <p>
                {{ Auth::user()->name }} -
                @foreach (Auth()->user()->getRoleNames() as $role)
                {{ strtoupper($role) }}
                @endforeach
                {{-- <small>Member since Nov. 2012</small> --}}
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="{{ route('profile.edit', Auth::User()->id) }}" class="btn btn-default btn-flat">โปรไฟล์</a>
              </div>
              <div class="pull-right">
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                  document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">ออกจากระบบ</a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>