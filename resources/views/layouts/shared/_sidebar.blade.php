<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('/uploads/avatar') }}/{{Auth()->user()->avatar ?? 'user-icon.png'}}"
                    class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth()->user()->name }}</p>
                <a href="#">
                    @foreach (Auth()->user()->getRoleNames() as $roleName)
                    <li>{{ strtoupper($roleName) }}</li>
                    @endforeach
                </a>
            </div>
        </div>
        <!-- search form -->
        <form action="{{ route('result') }}" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="subject" class="form-control" placeholder="ค้นหา">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i
                            class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            {{-- <li class="header">เมนูหลัก</li> --}}
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-folder-open-o"></i> <span>หนังสือเข้า</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('view inbox unreceive menu')
                    <li><a href="{{ route('unreceive.index') }}"><i class="fa fa-circle-o"></i>
                            หนังสือรอลงทะเบียนรับ
                        </a>

                    </li>
                    @endcan

                    @can('view inbox receive menu')
                    <li><a href="{{ route('receive.index') }}"><i class="fa fa-circle-o"></i>
                            หนังสือลงทะเบียนรับแล้ว</a></li>
                    @endcan

                    @can('view inbox reject menu')
                    <li><a href="{{ route('reject.index') }}"><i class="fa fa-circle-o"></i> หนังสือที่ถูกตีกลับ</a>
                    </li>
                    @endcan
                </ul>
            </li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-folder-open-o"></i> <span>หนังสือส่ง</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @can('view outbox unsend menu')
                    <li><a href="{{ route('unsend.index') }}"><i class="fa fa-circle-o"></i> หนังสือรอลงทะเบียนส่ง
                        </a>
                    </li>
                    @endcan

                    @can('view outbox send menu')
                    <li><a href="{{ route('send.index') }}"><i class="fa fa-circle-o"></i> หนังสือลงทะเบียนส่งแล้ว</a>
                    </li>
                    @endcan

                    @can('view outbox reject-send menu')
                    <li><a href="{{ route('reject-send.index') }}"><i class="fa fa-circle-o"></i>
                            หนังสือที่ถูกตีกลับ</a></li>
                    @endcan
                </ul>
            </li>
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-search"></i> <span>สืบค้นและรายงานหนังสือ</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    @can('view search menu')
                    <li><a href="{{ route('search') }}"><i class="fa fa-circle-o"></i> สืบค้นหนังสือ</a></li>
                    {{-- <li><a href="index2.html"><i class="fa fa-circle-o"></i> รายงานหนังสือเข้า</a></li>
                    <li><a href="index2.html"><i class="fa fa-circle-o"></i> รายงานหนังสือออก</a></li> --}}
                    @endcan

                </ul>
            </li>

            @can('view admin menu')
            <li class="active treeview">
                <a href="#">
                    <i class="fa fa-cogs"></i> <span>จัดการระบบ</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">

                    @can('view user manage menu')
                    <li><a href="{{ route('user.index') }}"><i class="fa fa-users"></i> จัดการผู้ใช้งานระบบ</a></li>
                    @endcan
                    @can('view role menu')
                    <li><a href="{{ route('role.index') }}"><i class="fa fa-check-circle-o"></i>
                            จัดการบทบาทของผู้ใช้งานระบบ</a></li>
                    @endcan

                </ul>
            </li>
            @endcan

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>