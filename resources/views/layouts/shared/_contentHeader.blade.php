<section class="content-header">
    <h1>
        {{ $title ?? 'untitled' }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ $title ?? 'untitled' }}</li>
    </ol>
</section>