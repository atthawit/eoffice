<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ผู้ปฏิบัติงานเข้าสู่ระบบ - ระบบสารบรรณอิเล็กทรอนิกส์ องค์การบริหารส่วนตำบลท่าข้าม</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/AdminLTE/dist/css/AdminLTE.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/iCheck/square/blue.css') }}">

    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Kanit|Sarabun&display=swap" rel="stylesheet">

    <!-- Embed Style -->
    <style>
        .login-logo {
            font-family: 'Kanit', sans-serif;
            font-size: 25px;
        }

        .image-logo {
            display: block;
            width: 80px;
            height: 80px;
            margin: auto;
            margin-bottom: 20px;
        }

        .login-box-body {
            font-family: 'Sarabun', sans-serif;
        }
    </style>
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('/') }}"><b>ระบบสารบรรณอิเล็กทรอนิกส์</b></a>
            <a href="{{ url('/') }}"><b>องค์การบริหารส่วนตำบลท่าข้าม</b></a>
        </div>
        <div class="login-box-body">
            {{-- <p class="login-box-msg">Sign in to start your session</p> --}}
            <img class="image-logo" src="{{ asset('/uploads/logo/thakam-logo.png') }}" alt="" />

            <form action="{{ route('login') }}" method="POST">
                @csrf
                <div class="form-group has-feedback">
                    <input type="email" id="email" name="email" class="form-control" placeholder="อีเมล"
                        value="{{ old('email') }}" required autocomplete="email" autofocus>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="รหัสผ่าน" name="password" required
                        autocomplete="current-password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox" name="remember" id="remember"
                                    {{ old('remember') ? 'checked' : '' }}> จดจำการเข้าสู่ระบบ
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">เข้าสู่ระบบ</button>
                    </div>
                </div>
            </form>

            <a href="{{ route('password.request') }}">ลืมรหัสผ่าน</a><br>

            <div class="login_with_thakam_passport">
                <hr class="style" />
                <div class="thakam-btn">
                    <a href="/thakam-passport/redirect" target="_blank"><img
                            src="{{ asset('images/thakam-btn.png') }}"></a>
                </div>
            </div>

        </div>
    </div>

    <!-- jQuery 3 -->
    <script src="{{ asset('/AdminLTE/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{ asset('/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- iCheck -->
    <script src="{{ asset('/AdminLTE/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%'
    });
  });
    </script>
</body>

</html>