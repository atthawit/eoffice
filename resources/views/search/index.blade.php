@extends('layouts.master')

@section('title')
สืบค้นหนังสือ
@endsection

@section('external-css')
<link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/iCheck/all.css') }}">
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'สืบค้นหนังสือ'])
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="{{ route('result') }}" method="get" class="form-horizontal">
                <div class="box-body">
                    <div class="form-group search-box {{ $errors->has('subject') ? 'has-error' : '' }}">
                        <label for="subject" class="col-sm-2 control-label">ชื่อเรื่องหนังสือ</label>

                        <div class="col-sm-10">
                            <div class="input-group keyword-box-default">
                                <input type="text" class="form-control" id="subject" name="subject"
                                    placeholder="ชื่อเรื่องหนังสือ">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-primary btn-flat">สืบค้นหนังสือ</button>
                                </span>
                            </div>

                            @if ($errors->has('keyword'))
                            <div class="help-block">
                                <strong>{{ $errors->first('keyword') }}</strong>
                            </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <input type="checkbox" value="inboxchecked" class="minimal" checked="" name="folder[]"
                                id="check-inbox">
                            <label for="check-inbox" style="cursor: pointer">หนังสือเข้า</label>

                            <input type="checkbox" value="outboxchecked" class="minimal" checked="" name="folder[]"
                                id="check-outbox">
                            <label for="check-outbox" style="cursor: pointer">หนังสือออก</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-block btn-flat advance-search-btn">การสืบค้นขั้นสูง
                                <span class="pull-right fa-toggle"><i class="fa fa-plus"></i></span></button>
                        </div>
                    </div>



                    <div class="advance-search">
                        <div class="form-group {{ $errors->has('receive_number') ? 'has-error' : '' }}">
                            <label for="receive_number" class="col-sm-2 control-label">เลขรับหนังสือ</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="number" name="receive_number" autofocus>
                                @if ($errors->has('receive_number'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('receive_number') }}</strong>
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group" {{ $errors->has('date') ? 'has-error' : '' }}>
                            <label for="date-start" class="col-sm-2 control-label">ช่วงวันที่</label>

                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="date-start">
                                            <input type="text" class="form-control" id="date-start" name="date_start"
                                                autocomplete="off">
                                        </div>
                                    </div>
                                    <label for="date-end" class="col-sm-2 control-label">ถึงวันที่</label>
                                    <div class="col-sm-5">
                                        <div class="date-end">
                                            <input type="text" class="form-control" id="date-end" name="date_end"
                                                autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('number') ? 'has-error' : '' }}">
                            <label for="number" class="col-sm-2 control-label">เลขที่หนังสือ</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="number" name="number"
                                    placeholder="เลขที่หนังสือ">
                                @if ($errors->has('number'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('number') }}</strong>
                                </div>
                                @endif
                            </div>
                        </div>



                        <div class="form-group {{ $errors->has('priority') ? 'has-error' : '' }}">
                            <label for="priority" class="col-sm-2 control-label">ชั้นความสำคัญ</label>

                            <div class="col-sm-10">
                                <select class="form-control" id="priority" name="priority">
                                    <option value="">เลือกชั้นความสำคัญ</option>
                                    @foreach ($priorities as $priority)

                                    <option value="{{ $priority->id }}">
                                        {{ $priority->priority_name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('priority'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('priority') }}</strong>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('secret_level') ? 'has-error' : '' }}">
                            <label for="secret_level" class="col-sm-2 control-label">ชั้นความลับ</label>

                            <div class="col-sm-10">
                                <select class="form-control" id="secret_level" name="secret_level">
                                    <option value="">เลือกชั้นความลับ</option>
                                    @foreach ($secretLevels as $secretLevel)
                                    <option value="{{ $secretLevel->id }}">
                                        {{ $secretLevel->secret_level_name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('secret_level'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('secret_level') }}</strong>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                            <label for="type" class="col-sm-2 control-label">ประเภทหนังสือ</label>

                            <div class="col-sm-10">
                                <select class="form-control" id="type" name="type">
                                    <option value="">เลือกประเภทหนังสือ</option>
                                    @foreach ($types as $type)
                                    <option value="{{ $type->id }}">
                                        {{ $type->type_name }}</option>
                                    {{-- <option value="{{$type->id }}">{{ $type->type_name }}</option> --}}
                                    @endforeach
                                </select>
                                @if ($errors->has('type'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('type') }}</strong>
                                </div>
                                @endif
                            </div>
                        </div>




                        <div class="form-group {{ $errors->has('source') ? 'has-error' : '' }}">
                            <label for="source" class="col-sm-2 control-label">จากหน่วยงาน</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="source" name="source"
                                    placeholder="จากหน่วยงาน">
                                @if ($errors->has('source'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('source') }}</strong>
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('destination') ? 'has-error' : '' }}">
                            <label for="destination" class="col-sm-2 control-label">เรียน / ถึง</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="destination" name="destination"
                                    placeholder="เรียน / ถึง">
                                @if ($errors->has('destination'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('destination') }}</strong>
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('purpose') ? 'has-error' : '' }}">
                            <label for="purpose" class="col-sm-2 control-label">การปฏิบัติ</label>

                            <div class="col-sm-10">
                                <select class="form-control" id="purpose" name="purpose">
                                    <option value="">เลือกการปฏิบัติ</option>
                                    @foreach ($purposes as $purpose)
                                    <option value="{{ $purpose->id }}">
                                        {{ $purpose->purpose_name }}
                                    </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('purpose'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('purpose') }}</strong>
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('keyword') ? 'has-error' : '' }}">
                            <label for="keyword" class="col-sm-2 control-label">คำค้น</label>

                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="keyword" name="keyword" placeholder="คำค้น">
                                @if ($errors->has('keyword'))
                                <div class="help-block">
                                    <strong>{{ $errors->first('keyword') }}</strong>
                                </div>
                                @endif
                            </div>
                        </div>




                        <div class="box-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit"
                                        class="btn btn-primary btn-block btn-flat">สืบค้นหนังสือ</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                @section('script')
                <script>
                    //Date picker
                        $('#date-start').datepicker({
                            autoclose: true,
                            format: 'dd/mm/yyyy'
                        })
                </script>
                <script>
                    //Date picker
                        $('#date-end').datepicker({
                            autoclose: true,
                            format: 'dd/mm/yyyy'
                        })
                </script>
                <script>
                    $(document).ready(function() {
      $(".add-more").click(function(){ 
          var html = $(".copy").html();
          $(".after-add-more").after(html);
      });
      $("body").on("click",".remove",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
                </script>

                <script>
                    $(document).ready(function(){
                        var html_keyword_box_default = $('.search-box').children().children('.keyword-box-default');
                        var html_keyword_box_default_clone = html_keyword_box_default;

                        var html_keyword_box = '<input type="text" class="form-control keyword-box" id="subject" name="subject" placeholder="ชื่อเรื่องหนังสือ">';
                        var html_keyword_box_clone = html_keyword_box;

                        $('.advance-search').hide();

                        $(".advance-search-btn").click(function(){
                            
                            $(".advance-search").slideToggle(function(){
                                if($('.fa-toggle').children().hasClass('fa-plus')) {
                                    $('.fa-toggle').children().removeClass('fa-plus');
                                    $('.fa-toggle').children().addClass('fa-minus');
                                } else {
                                    $('.fa-toggle').children().removeClass('fa-minus');
                                    $('.fa-toggle').children().addClass('fa-plus');
                                }

                                if($('.search-box').children().children().hasClass('keyword-box-default')){
                                    html_keyword_box_default.remove();
                                    $('.search-box').children('.col-sm-10').html(html_keyword_box);
                                }
                                else{
                                    $('.search-box').children('.col-sm-10').html(html_keyword_box_default_clone);
                                }
                            });
                        });
                    });
                </script>
                <script>
                    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                        checkboxClass: 'icheckbox_minimal-blue',
                        radioClass   : 'iradio_minimal-blue'
                        })
                </script>
                @endsection

                @section('external-js')
                <script src="{{ asset('/AdminLTE/plugins/iCheck/icheck.js') }}"></script>
                @endsection


            </form>
        </div>
    </div>
</div>
@endsection