@extends('layouts.master')

@section('title')
ผลลัพธ์การค้นหา
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'ผลลัพธ์การค้นหา ทั้งหมด ' . count($documents) . ' รายการ'])
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                {{-- <div class="box-tools">
                    <div class="input-group input-group-sm hidden-xs" style="width: 250px;">
                        <input type="text" name="table_search" class="form-control pull-right"
                            placeholder="สืบค้นหนังสือ">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div> --}}
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px" class="text-center">#</th>
                            <th class="text-center">ชนิดหนังสือ</th>
                            <th class="text-center">ลงวันที่</th>
                            <th class="text-center">ชั้นความสำคัญ</th>
                            <th class="text-center">ชั้นความลับ</th>
                            <th class="text-center">เรื่อง</th>
                            <th class="text-center">เลขที่หนังสือ</th>
                            <th class="text-center">เอกสารแนบ</th>
                            <th class="text-center">รายละเอียดเพิ่มเติม</th>
                        </tr>
                        @if (count($documents) == 0)
                        <tr>
                            <td colspan="8" class="text-center">ไม่พบผลลัพธ์การค้นหา</td>
                        </tr>
                        @else
                        @foreach ($documents as $document)
                        <tr>
                            <td class="text-center">{{$loop->iteration}}</td>
                            <td class="text-center"><strong>{{$document->progress->progress_name}}</strong></td>
                            <td class="text-center">{{$document->document_created_date}}</td>
                            <td class="text-center">{{$document->priority->priority_name}}</td>
                            <td class="text-center {{ $document->secret_class }}">
                                {{$document->secretLevel->secret_level_name}}</td>
                            <td class="text-center">@if ($document->secret)
                                {{$document->subject}}
                                @else
                                <span class="text-red" title="เอกสารที่คุณไม่มีสิทธิ์เข้าถึง"><i
                                        class="fa fa-expeditedssl" style="color: #ffb300"
                                        title="เอกสารที่คุณไม่มีสิทธิ์เข้าถึง"></i>
                                    เอกสารที่คุณไม่มีสิทธิ์เข้าถึง</span>
                                @endif</td>
                            <td class="text-center">{{$document->document_number}}</td>
                            <td>
                                @if (empty($document->attachment))
                                <p>ไม่มีเอกสารแนบ</p>
                                @else
                                {{-- <a href="{{$document->attachment}}"><i class="fa fa-file-pdf-o"></i>
                                ดาวน์โหลด</a> --}}
                                @if ($document->secret)
                                @foreach ($document->attachment as $attachment)
                                <a href="{{ asset('/uploads/documents') . '/' . $attachment['store_name'] }}"
                                    style="display:block"><i class="fa fa-file-pdf-o"></i>
                                    {{ $attachment['original_name'] }}</a>
                                @endforeach

                                @else
                                @foreach ($document->attachment as $attachment)
                                <i class="fa fa-expeditedssl" style="color: #ffb300"
                                    title="เอกสารแนบที่คุณไม่มีสิทธิ์เข้าถึง"></i>
                                <a href="#" style="display:block" title="เอกสารแนบที่คุณไม่มีสิทธิ์เข้าถึง"><i
                                        class="fa fa-file-pdf-o"></i>
                                    เอกสารแนบที่คุณไม่มีสิทธิ์เข้าถึง [{{ $loop->iteration }}]</a>
                                @endforeach
                                @endif
                                @endif
                            </td>
                            @php
                            $progresses = [ '1' => 'unreceive', '2' => 'unsend', '3' => 'receive', '4' => 'reject', '5'
                            => 'send', '6' => 'reject-send']
                            @endphp
                            <td class="text-center">
                                @if ($document->secret)
                                <a
                                    href="/documents/{{ $progresses[$document->document_progress_id] }}/{{ $document->id }}">รายละเอียดเพิ่มเติม</a>
                                @else
                                <a href="#" title="เอกสารที่คุณไม่มีสิทธิ์เข้าถึง"><i class="fa fa-expeditedssl"
                                        style="color: #ffb300" title="เอกสารที่คุณไม่มีสิทธิ์เข้าถึง"></i>
                                    รายละเอียดเพิ่มเติม</a>
                                @endif
                            </td>

                        </tr>
                        @endforeach
                        @endif

                    </tbody>
                </table>


            </div>

            <!-- /.box-body -->
            <div class="box-footer clearfix">
                {{-- <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">«</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">»</a></li>
                </ul> --}}
                <div class="pull-right">
                    {{ $documents->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>

</script>
@endsection