@extends('layouts.master')

@section('title')
แก้ไขหนังสือรอลงทะเบียนส่ง
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'แก้ไขหนังสือรอลงทะเบียนส่ง'])
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="{{ route('unsend.update', $document->id) }}" method="POST" class="form-horizontal"
                enctype="multipart/form-data">
                {{ method_field('PATCH') }}
                @include('outbox.unsend._form', ['buttonText' => "แก้ไขหนังสือรอลงทะเบียนส่ง"])
            </form>
        </div>
    </div>
</div>
@endsection