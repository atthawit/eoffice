@extends('layouts.master')

@section('title')
สร้างหนังสือรอลงทะเบียนส่ง
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'สร้างหนังสือรอลงทะเบียนส่ง'])
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="{{ route('unsend.store') }}" method="POST" class="form-horizontal"
                enctype="multipart/form-data">
                @include('outbox.unsend._form', ['buttonText' => "เพิ่มหนังสือรอลงทะเบียนส่ง"])
            </form>
        </div>
    </div>
</div>
@endsection