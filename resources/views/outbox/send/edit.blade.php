@extends('layouts.master')

@section('title')
แก้ไขหนังสือลงทะเบียนส่งแล้ว
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'แก้ไขหนังสือลงทะเบียนส่งแล้ว'])
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="{{ route('send.update', $document->id) }}" method="POST" class="form-horizontal"
                enctype="multipart/form-data">
                {{ method_field('PATCH') }}
                @include('outbox.send._form', ['buttonText' => "แก้ไขหนังสือลงทะเบียนส่งแล้ว"])
            </form>
        </div>
    </div>
</div>
@endsection