@extends('layouts.master')

@section('title')
หนังสือที่ถูกตีกลับ
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'หนังสือที่ถูกตีกลับ'])
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <div class="box-tools">
                    <form action="/documents/search/result" method="GET">
                        <div class="input-group input-group-sm hidden-xs" style="width: 250px;">
                            <input type="text" name="subject" class="form-control search-box pull-right"
                                placeholder="สืบค้นหนังสือ">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px" class="text-center">#</th>
                            <th class="text-center">วันที่ตีกลับ</th>
                            <th class="text-center">เลขที่หนังสือ</th>
                            <th class="text-center">ลงวันที่</th>
                            <th class="text-center">ชั้นความสำคัญ</th>
                            <th class="text-center">ชั้นความลับ</th>
                            <th class="text-center">เรื่อง</th>

                            <th class="text-center">เอกสารแนบ</th>

                            <th class="text-center">ผู้ตีกลับ</th>
                            <th class="text-center">เหตุผลการตีกลับ</th>
                            <th class="text-center">รายละเอียดเพิ่มเติม</th>
                            @canany(['edit document','delete document'])
                            <th class="text-center">แก้ไข / ลบ</th>
                            @endcanany
                        </tr>
                        @if (count($documents) == 0)
                        <tr>
                            <td colspan="12" class="text-center">ไม่มีหนังสือที่ตีกลับ</td>
                        </tr>
                        @else
                        @foreach ($documents as $document)
                        <tr>
                            <td class="text-center">{{$loop->iteration}}</td>
                            <td class="text-center">{{$document->document_reject_date_thai}}</td>
                            <td class="text-center">{{$document->document_number_thai}}</td>
                            <td class="text-center">{{ $document->document_created_date_thai }}</td>
                            <td class="text-center">{{$document->priority->priority_name}}</td>
                            <td class="text-center {{ $document->secret_class }}">
                                {{$document->secretLevel->secret_level_name}}</td>
                            <td class="text-center">
                                @if ($document->secret)
                                <strong>{{$document->subject}}</strong>
                                @else
                                <span class="text-red" title="เอกสารที่คุณไม่มีสิทธิ์เข้าถึง"><i
                                        class="fa fa-expeditedssl" style="color: #ffb300"
                                        title="เอกสารที่คุณไม่มีสิทธิ์เข้าถึง"></i>
                                    เอกสารที่คุณไม่มีสิทธิ์เข้าถึง</span>
                                @endif</td>

                            <td>
                                @if (count($document->attachments) == 0)
                                <p>ไม่มีเอกสารแนบ</p>
                                @else

                                @if ($document->secret)
                                @foreach ($document->attachments as $attachment)
                                <a href="{{ asset('/uploads/documents') . '/' . $attachment['store_name'] }}"
                                    style="display:block"><i class="fa fa-file-pdf-o"></i>
                                    {{ $attachment['original_name'] }}</a>
                                @endforeach

                                @else
                                @foreach ($document->attachments as $attachment)
                                <i class="fa fa-expeditedssl" style="color: #ffb300"
                                    title="เอกสารแนบที่คุณไม่มีสิทธิ์เข้าถึง"></i>
                                <a href="#" style="display:block" title="เอกสารแนบที่คุณไม่มีสิทธิ์เข้าถึง"><i
                                        class="fa fa-file-pdf-o"></i>
                                    เอกสารแนบที่คุณไม่มีสิทธิ์เข้าถึง [{{ $loop->iteration }}]</a>
                                @endforeach
                                @endif

                                @endif
                            </td>

                            <td class="text-center">{{ $document->userAction->name }}</td>
                            <td class="text-center">{{ $document->document_reject_reason }}</td>

                            <td class="text-center">
                                @if ($document->secret)
                                <a href="{{ route('reject-send.show', $document->id) }}">รายละเอียดเพิ่มเติม</a>
                                @else
                                <a href="#" title="เอกสารที่คุณไม่มีสิทธิ์เข้าถึง"><i class="fa fa-expeditedssl"
                                        style="color: #ffb300" title="เอกสารที่คุณไม่มีสิทธิ์เข้าถึง"></i>
                                    รายละเอียดเพิ่มเติม</a>
                                @endif
                            </td>

                            @canany(['edit document', 'delete document'])
                            <td class="text-center">
                                @can('edit document')
                                <a href="{{ route('reject-send.edit', $document->id) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @endcan
                                @can('delete document')
                                <a href="#" class="btn btn-default btn-xs delete-link" data-toggle="modal"
                                    data-target="#modal-delete" data-id="{{$document->id}}"
                                    data-number="{{ $document->document_number }}">
                                    <i class=" fa fa-trash"></i>
                                </a>
                                @endcan
                            </td>
                            @endcanany


                        </tr>
                        @endforeach
                        @endif

                    </tbody>
                </table>
            </div>

            @include('layouts.shared.modals._delete')
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                {{-- <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">«</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">»</a></li>
                </ul> --}}
                <div class="pull-right">
                    {{ $documents->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function() {
                $(document).on("click", ".delete-link", function () {
                    var document_id = $(this).data('id');
                    var document_number = $(this).data('number');
    
                    $('#delete-form').attr('action', '/documents/reject-send/' + document_id);
                    $('.modal-title').text('ลบหนังสือ');
                    $('#delete-modal-number-content').html('<strong>ยืนยัน</strong>การลบหนังสือหมายเลข ' + document_number);                
            });
        });
</script>
<script>
    $(document).ready(function () {
                            var documents_suggestions = new Bloodhound({
                                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                                queryTokenizer: Bloodhound.tokenizers.whitespace,
                                remote: {
                                    url: '/typeahead-search/6/?keyword=%QUERY',
                                    wildcard: '%QUERY' // %QUERY will be replace by users input in
                                }, // the url option.
                            });
                    
                            // init Typeahead
                            $('.search-box').typeahead({
                                minLength: 1,
                                highlight: true
                            }, {
                                name: 'documents',
                                source: documents_suggestions,
                                display: function (item) {
                                    return item.subject;
                                },
                                limit: 5,
                                templates: {
                                    suggestion: function (item) {
                                        return '<div><a href="/documents/receive/' + item.id + '">' + item.subject + '</a></div>'
                                    }
                                }
                            });
                        }); 
</script>
@endsection