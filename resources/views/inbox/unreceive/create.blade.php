@extends('layouts.master')

@section('title')
สร้างหนังสือรอลงทะเบียนรับ
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'สร้างหนังสือรอลงทะเบียนรับ'])
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="{{ route('unreceive.store') }}" method="POST" class="form-horizontal"
                enctype="multipart/form-data">
                @include('inbox.unreceive._form', ['buttonText' => "เพิ่มหนังสือรอลงทะเบียนรับ"])
            </form>
        </div>
    </div>
</div>
@endsection