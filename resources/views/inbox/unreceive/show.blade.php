@extends('layouts.master')

@section('title')
รายละเอียดหนังสือเลขที่ {{ $document->document_number }}
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'รายละเอียดหนังสือเลขที่ ' . $document->document_number])
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">

            <div class="box-body table-responsive no-padding">
                <div class="col-md-10 col-md-offset-1">
                    <br />
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <td><strong>เลขที่หนังสือ</strong></td>
                                <td>{{ $document->document_number }}</td>
                            </tr>
                            <tr>
                                <td><strong>ลงวันที่</strong></td>
                                <td>{{ $document->document_created_date_thai }}</td>
                            </tr>
                            <tr>
                                <td><strong>ชั้นความสำคัญ</strong></td>
                                <td>{{ $document->priority->priority_name }}</td>
                            </tr>
                            <tr>
                                <td><strong>ชั้นความลับ</strong></td>
                                <td>{{ $document->secretLevel->secret_level_name }}</td>
                            </tr>
                            <tr>
                                <td><strong>เรื่อง</strong></td>
                                <td><strong>{{ $document->subject }}</strong></td>
                            </tr>
                            <tr>
                                <td><strong>ประเภทหนังสือ</strong></td>
                                <td>{{ $document->type->type_name }}</td>
                            </tr>
                            <tr>
                                <td><strong>เอกสารแนบ</strong></td>
                                <td>@if (count($document->attachments) == 0)
                                    <p>ไม่มีเอกสารแนบ</p>
                                    @else
                                    {{-- <a href="{{$document->attachment}}"><i class="fa fa-file-pdf-o"></i>
                                    ดาวน์โหลด</a> --}}
                                    @foreach ($document->attachments as $attachment)
                                    <a target="_blank"
                                        href="{{ asset('/uploads/documents') . '/' . $attachment['store_name'] }}"
                                        style="display:block"><i class="fa fa-file-pdf-o"></i>
                                        {{ $attachment['original_name'] }}</a>
                                    @endforeach
                                    @endif</td>
                            </tr>

                            <tr>
                                <td><strong>จากหน่วยงาน</strong></td>
                                <td>{{ $document->document_source }}</td>
                            </tr>
                            <tr>
                                <td><strong>เรียน / ถึง</strong></td>
                                {{-- <td>{{ $document->document_destination }}</td> --}}
                                <td>
                                    @if (empty($document->document_destination))
                                    <span></span>
                                    @else
                                    @foreach ($document->document_destination as $document_destination)
                                    <span>{{ $document_destination }}</span>
                                    @if (!$loop->last)
                                    ,
                                    @endif
                                    @endforeach
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><strong>การปฏิบัติ</strong></td>
                                <td>{{ $document->purpose->purpose_name }}</td>
                            </tr>
                            <tr>
                                <td><strong>แท็ก</strong></td>
                                <td>
                                    @if (empty($document->keyword))
                                    <span></span>
                                    @else
                                    @foreach ($document->keyword as $keyword)
                                    <a href="/documents/tag/{{ $keyword }}" target="_blank" class="label bg-navy"
                                        style="display: inline">{{ $keyword }}</a>
                                    @endforeach
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td><strong>หมายเหตุ</strong></td>
                                <td>{!! $document->annotation !!}</td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="{{ route('unreceive.index') }}" class="btn btn-primary btn-block">กลับหน้ารายการหนังสือ</a>
                    <br />
                </div>
            </div>
        </div>
    </div>
</div>
@endsection