@extends('layouts.master')

@section('title')
แก้ไขหนังสือรอลงทะเบียนรับ
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'แก้ไขหนังสือรอลงทะเบียนรับ'])
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="{{ route('unreceive.update', $document->id) }}" method="POST" class="form-horizontal"
                enctype="multipart/form-data">
                {{ method_field('PATCH') }}
                @include('inbox.unreceive._form', ['buttonText' => "แก้ไขหนังสือรอลงทะเบียนรับ"])
            </form>
        </div>
    </div>
</div>
@endsection