@extends('layouts.master')

@section('title')
หนังสือรอลงทะเบียนรับ
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'หนังสือรอลงทะเบียนรับ'])
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                @can('create document')
                <a href="{{ route('unreceive.create') }}" class="btn btn-default">สร้างหนังสือใหม่</a>
                @endcan
                <div class="box-tools">
                    <form action="/documents/search/result" method="GET">
                        <div class="input-group input-group-sm hidden-xs" style="width: 250px;">
                            <input type="text" name="subject" class="form-control search-box pull-right"
                                placeholder="สืบค้นหนังสือ">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px" class="text-center">#</th>
                            <th class="text-center">เลขที่หนังสือ</th>
                            <th class="text-center">ลงวันที่</th>
                            <th class="text-center">ชั้นความสำคัญ</th>
                            <th class="text-center">ชั้นความลับ</th>
                            <th class="text-center">เรื่อง</th>

                            <th class="text-center">เอกสารแนบ</th>
                            @canany(['accept document', 'reject document'])
                            <th class="text-center">ลงรับ / ตีกลับ</th>
                            @endcanany
                            <th class="text-center">รายละเอียดเพิ่มเติม</th>
                            @canany(['edit document', 'delete document'])
                            <th class="text-center">แก้ไข / ลบ</th>
                            @endcanany
                        </tr>
                        @if (count($documents) == 0)
                        <tr>
                            <td colspan="10" class="text-center">ไม่มีหนังสือที่รอลงทะเบียนรับ</td>
                        </tr>
                        @else
                        @foreach ($documents as $document)
                        <tr>
                            <td class="text-center">{{$loop->iteration}}</td>
                            <td class="text-center">{{$document->document_number_thai}}</td>
                            <td class="text-center">{{$document->document_created_date_thai}}</td>
                            <td class="text-center">{{$document->priority->priority_name}}</td>
                            <td class="text-center {{ $document->secret_class }}">
                                {{$document->secretLevel->secret_level_name}}</td>
                            <td class="text-center">
                                @if ($document->secret)
                                <strong>{{$document->subject}}</strong>
                                @else
                                <span class="text-red" title="เอกสารที่คุณไม่มีสิทธิ์เข้าถึง"><i
                                        class="fa fa-expeditedssl" style="color: #ffb300"
                                        title="เอกสารที่คุณไม่มีสิทธิ์เข้าถึง"></i>
                                    เอกสารที่คุณไม่มีสิทธิ์เข้าถึง</span>
                                @endif
                            </td>

                            <td>
                                @if (count($document->attachments) == 0)
                                <p>ไม่มีเอกสารแนบ</p>
                                @else

                                @if ($document->secret)
                                @foreach ($document->attachments as $attachment)
                                <a target="_blank"
                                    href="{{ asset('/uploads/documents') . '/' . $attachment['store_name'] }}"
                                    style="display:block"><i class="fa fa-file-pdf-o"></i>
                                    {{ $attachment['original_name'] }}</a>
                                @endforeach

                                @else
                                @foreach ($document->attachments as $attachment)
                                <i class="fa fa-expeditedssl" style="color: #ffb300"
                                    title="เอกสารแนบที่คุณไม่มีสิทธิ์เข้าถึง"></i>
                                <a href="#" style="display:block" title="เอกสารแนบที่คุณไม่มีสิทธิ์เข้าถึง"><i
                                        class="fa fa-file-pdf-o"></i>
                                    เอกสารแนบที่คุณไม่มีสิทธิ์เข้าถึง [{{ $loop->iteration }}]</a>
                                @endforeach
                                @endif

                                @endif
                            </td>


                            @canany(['accept document', 'reject document'])
                            <td class="text-center">
                                <div>@can('accept document')<a href="#" class="accept-link" data-toggle="modal"
                                        data-target="#modal-accept" data-id="{{$document->id}}"
                                        data-number="{{ $document->document_number }}" data-year="{{ \Carbon\Carbon::parse($document->created_at)->format('Y') }}
                                        ">ลงรับ</a>@endcan / @can('reject document')<a href="#" class="reject-link"
                                        data-toggle="modal" data-target="#modal-reject" data-id="{{$document->id}}"
                                        data-number="{{$document->document_number}}"
                                        data-source="{{$document->document_source}}"
                                        data-destination="{{$document->document_source}}">ตีกลับ</a>@endcan</div>
                            </td>
                            @endcanany()


                            @if ($document->secret)
                            <td class="text-center"><a
                                    href="{{ route('unreceive.show', $document->id) }}">รายละเอียดเพิ่มเติม</a></td>
                            @else
                            <td class="text-center">
                                <a href="#" title="เอกสารที่คุณไม่มีสิทธิ์เข้าถึง"><i class="fa fa-expeditedssl"
                                        style="color: #ffb300" title="เอกสารที่คุณไม่มีสิทธิ์เข้าถึง"></i>
                                    รายละเอียดเพิ่มเติม</a></td>
                            @endif

                            @canany(['edit document', 'delete document'])
                            <td class="text-center">
                                @can('edit document')
                                <a href="{{ route('unreceive.edit', $document->id) }}" class="btn btn-default btn-xs">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @endcan

                                @can('delete document')
                                <a href="#" class="btn btn-default btn-xs delete-link" data-toggle="modal"
                                    data-target="#modal-delete" data-id="{{$document->id}}"
                                    data-number="{{ $document->document_number }}">
                                    <i class=" fa fa-trash"></i>
                                </a>
                                @endcan
                            </td>
                            @endcanany
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>

            <!-- Modal -->
            @include('layouts.shared.modals._reject')
            @include('layouts.shared.modals._accept_with_receive_number')
            @include('layouts.shared.modals._delete')


            <!-- /.box-body -->
            <div class="box-footer clearfix">
                {{-- <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">«</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">»</a></li>
                </ul> --}}
                <div class="pull-right">
                    {{ $documents->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $( document ).ready(function() {
            $(document).on("click", ".reject-link", function () {
                var document_id = $(this).data('id');
                var document_number = $(this).data('number');
                var document_source = $(this).data('source');
                var document_destination = $(this).data('destination');

                $('#reject-form').attr('action', '/documents/unreceive/' + document_id + '/reject');
                $('.modal-title').text('ตีกลับหนังสือ');
                $('#reject-modal-source-content').text(document_source);
                $('#reject-modal-destination-content').text(document_destination);
                $('#reject-modal-number-content').text(document_number);
        });
    });
</script>
<script>
    $( document ).ready(function() {
            var document_year = '';
            $('#accept-btn').prop('disabled', true);

            $(document).on("click", ".accept-link", function () {
                var document_id = $(this).data('id');
                document_year = $(this).data('year');
                var document_number = $(this).data('number');
                


                $('#accept-form').attr('action', '/documents/unreceive/' + document_id + '/accept');
                $('.modal-title').text('ลงรับหนังสือ');
                $('#accept-modal-number-content').html('<strong>ยืนยัน</strong>การลงรับหนังสือหมายเลข ' + document_number); 
                $('#accept-btn').text('ลงรับหนังสือ');      
                    
        });
                var receive_number;
                var receive_number_with_year;
                var _token = $("input[name='_token']").val();

                function clear_alert(){
                    $('#alert_validate').addClass('hide');
                    $('#alert_validate').hasClass('alert-success') ? $('#alert_validate').removeClass('alert-success') : $('#alert_validate').removeClass('alert-danger');
                    $('#alert_validate').text('');
                }

                $('#receive_number').on('input', function() {
                    clear_alert();
                    function add_success_class(data){
                        $('#alert_validate').removeClass('hide');
                        $('#alert_validate').addClass('alert-success');
                        $('#alert_validate').text(data.success);
                    }

                    function add_danger_class(data){
                        $('#alert_validate').removeClass('hide');
                        $('#alert_validate').addClass('alert-danger');
                        $('#alert_validate').text(data.error);
                    }

                    
                    receive_number = $('#receive_number').val();
                    receive_number_with_year = receive_number + '_' + document_year;
                    $('#receive_number_with_year').val(receive_number_with_year);   

                    if($(this).val() != '') {
                        $('#accept-btn').prop('disabled', false);
                        
                        $.ajax({
                            url: "/documents/helper/validate_receive_number",
                            async: false,
                            type:'POST',
                            data: {_token:_token, receive_number_with_year:receive_number_with_year},
                            success: function(data) {
                                if($.isEmptyObject(data.error)){
                                    add_success_class(data);
                                }else{
                                    add_danger_class(data);
                                    $('#accept-btn').prop('disabled', true);
                                }
                            }
                        });
                    }
                    else {
                        $('#accept-btn').prop('disabled', true);  
                    }
                });
    });
</script>
<script>
    $( document ).ready(function() {
            $(document).on("click", ".delete-link", function () {
                var document_id = $(this).data('id');
                var document_number = $(this).data('number');

                $('#delete-form').attr('action', '/documents/unreceive/' + document_id);
                $('.modal-title').text('ลบหนังสือ');
                $('#delete-modal-number-content').html('<strong>ยืนยัน</strong>การลบหนังสือหมายเลข ' + document_number);                
        });
    });
</script>
<script>
    $(document).ready(function () {
            var documents_suggestions = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '/typeahead-search/1/?keyword=%QUERY',
                    wildcard: '%QUERY' // %QUERY will be replace by users input in
                }, // the url option.
            });
    
            // init Typeahead
            $('.search-box').typeahead({
                minLength: 1,
                highlight: true
            }, {
                name: 'documents',
                source: documents_suggestions,
                display: function (item) {
                    return item.subject;
                },
                limit: 5,
                templates: {
                    suggestion: function (item) {
                        return '<div><a href="/documents/unreceive/' + item.id + '">' + item.subject + '</a></div>'
                    }
                }
            });
        }); 
</script>
@endsection