@section('external-css')
<link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/select2/dist/css/select2.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
@endsection

@csrf
<div class="box-body">
    <div class="form-group {{ $errors->has('number') ? 'has-error' : '' }}">
        <label for="number" class="col-sm-2 control-label">เลขที่หนังสือ</label>

        <div class="col-sm-10">
            <input type="text" class="form-control" id="number" name="number" placeholder="เลขที่หนังสือ"
                value="{{ old('number', $document->document_number) }}" autofocus>
            @if ($errors->has('number'))
            <div class="help-block">
                <strong>{{ $errors->first('number') }}</strong>
            </div>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('subject') ? 'has-error' : '' }}">
        <label for="subject" class="col-sm-2 control-label">เรื่อง</label>

        <div class="col-sm-10">
            <input type="subject" class="form-control" id="subject" name="subject" placeholder="เรื่อง"
                value="{{ old('subject', $document->subject) }}">
            @if ($errors->has('subject'))
            <div class="help-block">
                <strong>{{ $errors->first('subject') }}</strong>
            </div>
            @endif
        </div>
    </div>

    <div class="form-group {{ $errors->has('priority') ? 'has-error' : '' }}">
        <label for="priority" class="col-sm-2 control-label">ชั้นความสำคัญ</label>

        <div class="col-sm-10">
            <select class="form-control" id="priority" name="priority">
                <option value="">เลือกชั้นความสำคัญ</option>
                @foreach ($priorities as $priority)

                <option value="{{ $priority->id }}"
                    {{ (((old("priority") == $priority->id) or ($priority->id == $document->priority_id)) ? "selected":"") }}>
                    {{ $priority->priority_name }}</option>
                @endforeach
            </select>
            @if ($errors->has('priority'))
            <div class="help-block">
                <strong>{{ $errors->first('priority') }}</strong>
            </div>
            @endif
        </div>
    </div>
    <div class="form-group {{ $errors->has('secret_level') ? 'has-error' : '' }}">
        <label for="secret_level" class="col-sm-2 control-label">ชั้นความลับ</label>

        <div class="col-sm-10">
            <select class="form-control" id="secret_level" name="secret_level">
                <option value="">เลือกชั้นความลับ</option>
                @foreach ($secretLevels as $secretLevel)
                <option value="{{ $secretLevel->id }}"
                    {{ (((old("secret_level") == $secretLevel->id) or ($secretLevel->id == $document->secret_level_id)) ? "selected":"") }}>
                    {{ $secretLevel->secret_level_name }}</option>
                @endforeach
            </select>
            @if ($errors->has('secret_level'))
            <div class="help-block">
                <strong>{{ $errors->first('secret_level') }}</strong>
            </div>
            @endif
        </div>
    </div>
    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
        <label for="type" class="col-sm-2 control-label">ประเภทหนังสือ</label>

        <div class="col-sm-10">
            <select class="form-control" id="type" name="type">
                <option value="">เลือกประเภทหนังสือ</option>
                @foreach ($types as $type)
                @if ($type->id < 7) <option value="{{ $type->id }}"
                    {{ (((old("type") == $type->id) or ($type->id == $document->type_id)) ? "selected":"") }}>
                    {{ $type->type_name }}</option>
                    @endif
                    @endforeach
            </select>
            @if ($errors->has('type'))
            <div class="help-block">
                <strong>{{ $errors->first('type') }}</strong>
            </div>
            @endif
        </div>
    </div>
    @if (Route::current()->getName() === 'unreceive.edit')
    <div class="form-group">
        <label for="type" class="col-sm-2 control-label"> </label>

        <div class="col-sm-10">
            <p>เอกสารแนบปัจจุบัน</p>
            @if (count($document->attachments) == 0)
            <p>-</p>
            @else
            @foreach ($document->attachments as $attachment)
            <div class="input-group" style="margin-top: 10px;">

                <input type="text" class="form-control" value="{{ $attachment['original_name'] }}" disabled>
                <div class="input-group-btn">
                    <button class="btn btn-danger ca-delete" data-id="{{ $attachment['id'] }} "><i
                            class="fa fa-close"></i>
                        ลบ</button>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
    @endif

    {{-- <div class="form-group {{ $errors->has('attachment') ? 'has-error' : '' }}">
    <label for="attachment" class="col-sm-2 control-label">เอกสารแนบ</label>

    <div class="col-sm-10">
        <input type="file" class="form-control" id="attachment" name="attachment">
        @if ($errors->has('attachment'))
        <div class="help-block">
            <strong>{{ $errors->first('attachment') }}</strong>
        </div>
        @endif
    </div>
</div> --}}
<div class="form-group {{ $errors->has('attachment') ? 'has-error' : '' }}">
    <label for="type" class="col-sm-2 control-label">เอกสารแนบ</label>

    <div class="col-sm-10">
        <div class="input-group control-group after-add-more">
            <input type="file" name="attachment[]" class="form-control">
            <div class="input-group-btn">
                <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i>
                    เพิ่ม</button>
            </div>
            @if ($errors->has('attachment'))
            <div class="help-block">
                <strong>{{ $errors->first('attachment') }}</strong>
            </div>
            @endif
        </div>
    </div>
</div>

<!-- Copy Fields -->
<div class="form-group copy hide {{ $errors->has('attachment') ? 'has-error' : '' }}">
    <div class="control-group input-group" style="margin-top:10px">
        <input type="file" name="attachment[]" class="form-control">
        <div class="input-group-btn">
            <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i>
                ลบ</button>
        </div>
        @if ($errors->has('attachment'))
        <div class="help-block">
            <strong>{{ $errors->first('attachment') }}</strong>
        </div>
        @endif
    </div>
</div>

{{-- <div class="form-group {{ $errors->has('date') ? 'has-error' : '' }}">
<label for="date" class="col-sm-2 control-label">ลงวันที่</label>

<div class="col-sm-10">
    <input type="date" class="form-control" id="date" name="date"
        value="{{ old('date', $document->document_created_date) }}">
    @if ($errors->has('date'))
    <div class="help-block">
        <strong>{{ $errors->first('date') }}</strong>
    </div>
    @endif
</div>
</div> --}}

<div class="form-group" {{ $errors->has('date') ? 'has-error' : '' }}>
    <label for="date" class="col-sm-2 control-label">ลงวันที่</label>

    <div class="col-sm-10">
        <div class="date">
            <input type="text" class="form-control" id="date" name="date"
                value="{{ old('date', $document->document_created_date) }}" autocomplete="off">
        </div>
    </div>
</div>

<div class="form-group {{ $errors->has('source') ? 'has-error' : '' }}">
    <label for="source" class="col-sm-2 control-label">จากหน่วยงาน</label>

    <div class="col-sm-10">
        <input type="text" class="form-control" id="source" name="source" placeholder="จากหน่วยงาน"
            value="{{ old('source', $document->document_source) }}">
        @if ($errors->has('source'))
        <div class="help-block">
            <strong>{{ $errors->first('source') }}</strong>
        </div>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('destination') ? 'has-error' : '' }}">
    <label for="destination" class="col-sm-2 control-label">เวียนไปยังหน่วยงาน</label>

    <div class="col-sm-10">

        <select class="form-control" multiple="multiple" id="destination" name="destination[]"
            placeholder="เรียน / ถึง">
            @foreach ($roles as $role)
            <option value="{{ $role->name }}" @if (count((array) $document->document_destination) != 0)
                @foreach ($document->document_destination as $d)
                @if ($role->name == old('destination', $d))
                selected="selected"
                @endif
                @endforeach
                @endif
                >{{ $role->name }}</option>
            @endforeach
        </select>

        {{-- <input type="text" class="form-control" id="destination" name="destination" placeholder="เรียน / ถึง"
            value="{{ old('destination', $document->document_destination) }}"> --}}



        {{-- @if (old('destination', $document->document_destination))

        @endif --}}



        {{-- <select class="form-control" id="destination" name="destination"> --}}
        {{-- @php
            $roles_name = [];
            foreach($roles as $role) {
            array_push($roles_name, $role->name);
            }
            @endphp --}}

        {{-- <option value="">เรียน / ถึง</option> --}}


        {{-- @if (!in_array(old('destination', $document->document_destination), $roles_name))
            <option value="{{ old('destination', $document->document_destination) }}" selected>
        {{ old('destination', $document->document_destination) }}
        </option>
        @foreach ($roles as $role)
        <option value="{{ ($role->name) }}">
            {{ $role->name }}
        </option>
        @endforeach

        @else
        @foreach ($roles as $role)
        <option value="{{ ($role->name) }}"
            {{ (((old("destination") == $role->name) or ($role->name == $document->document_destination) ) ? "selected":"") }}>
            {{ $role->name }}
        </option>
        @endforeach
        @endif --}}


        {{-- </select> --}}





        @if ($errors->has('destination'))
        <div class="help-block">
            <strong>{{ $errors->first('destination') }}</strong>
        </div>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('purpose') ? 'has-error' : '' }}">
    <label for="purpose" class="col-sm-2 control-label">การปฏิบัติ</label>

    <div class="col-sm-10">
        <select class="form-control" id="purpose" name="purpose">
            <option value="">เลือกการปฏิบัติ</option>
            @foreach ($purposes as $purpose)
            <option value="{{ $purpose->id }}"
                {{ (((old("purpose") == $purpose->id) or ($purpose->id == $document->purpose_id)) ? "selected":"") }}>
                {{ $purpose->purpose_name }}
            </option>
            @endforeach
        </select>
        @if ($errors->has('purpose'))
        <div class="help-block">
            <strong>{{ $errors->first('purpose') }}</strong>
        </div>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('keyword') ? 'has-error' : '' }}">
    <label for="keyword" class="col-sm-2 control-label">คำค้น</label>

    <div class="col-sm-10">
        <select class="form-control" multiple="multiple" id="keyword" name="keyword[]" placeholder="คำค้น">
            @if (!empty($document->keyword))
            @foreach ($document->keyword as $keyword)
            <option value="{{$keyword}}" selected>{{ $keyword }}</option>
            @endforeach
            @endif
        </select>
        @if ($errors->has('keyword'))
        <div class="help-block">
            <strong>{{ $errors->first('keyword') }}</strong>
        </div>
        @endif
    </div>
</div>

<div class="form-group {{ $errors->has('annotation') ? 'has-error' : '' }}">
    <label for="annotation" class="col-sm-2 control-label">หมายเหตุ</label>

    <div class="col-sm-10">
        <div class="box-body pad">
            <textarea rows="5" class="form-control textarea" name="annotation" id="annotation"
                placeholder="หมายเหตุ">{{ old('annotation', $document->annotation) }}</textarea>
        </div>
        @if ($errors->has('annotation'))
        <div class="help-block">
            <strong>{{ $errors->first('annotation') }}</strong>
        </div>
        @endif
    </div>
</div>
</div>

<div class="box-footer">
    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('unreceive.index') }}" class="btn btn-default btn-block">ยกเลิก</a>
        </div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary pull-right btn-block">{{ $buttonText }}</button>
        </div>
    </div>
</div>
@section('external-js')
<script src="{{ asset('/AdminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


@endsection

@section('script')
<script>
    function getAjaxErrorDetail(jqXHR, exception) {
	var msg = '';
	if (jqXHR.status === 0) {
		msg = 'Not connect.\n Verify Network.';
	} else if (jqXHR.status == 404) {
		msg = 'Requested page not found. [404]';
	} else if (jqXHR.status == 500) {
		msg = 'Internal Server Error [500].';
	} else if (exception === 'parsererror') {
		msg = 'Requested JSON parse failed.';
	} else if (exception === 'timeout') {
		msg = 'Time out error.';
	} else if (exception === 'abort') {
		msg = 'Ajax request aborted.';
	} else {
		msg = 'Uncaught Error.\n' + jqXHR.responseText;
	}

	return msg;
}

function Toast(type, css, msg) {
	this.type = type;
	this.css = css;
	this.msg = msg;
}

toastr.options = {
	"positionClass": 'toast-top-full-width',
	"closeButton": true,
	"timeOut": 5000,
	"fadeOut": 250,
	"fadeIn": 250,
}

function showToast(toasts) {
	var t = toasts;
	toastr.options.positionClass = t.css;
	toastr[t.type](t.msg);
}

$( document ).ready(function() {
    $(".ca-delete").click(function (e) {
        e.preventDefault();

        var ca_id = $(this).data('id');
        var cur_obj = $(this);

        $.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
        });
        
        $.ajax({
			beforeSend: function () {
				// Handle the beforeSend event
			},
			complete: function () {
				// Handle the complete event
            },
            // {{ route('ajax.delete_attachment') }}
			url: "{{ route('ajax.delete_attachment') }}",
			method: 'post',
			data: {
				'_token': '{{ csrf_token() }}',
				'ca_id': ca_id
			},
			success: function (result) {
                if (result.success) {
                    cur_obj.parent().parent(".input-group").remove();
                    showToast(new Toast('success', 'toast-top-right', 'ลบเอกสารแนบสำเร็จ'));
                }
                else if (result.error) {
                    showToast(new Toast('error', 'toast-top-right', 'ลบเอกสารแนบไม่สำเร็จ โปรดลองอีกครั้ง'));
                }
			},
			error: function (jqXHR, exception) {
                var errorDetail = getAjaxErrorDetail(jqXHR, exception);
                showToast(new Toast('error', 'toast-top-right', 'เกิดข้อผิดพลาดในการลบเอกสารแนบ' + errorDetail));
			},
		});
    });
});
</script>
<script>
    $(document).ready(function() {
          $(".add-more").click(function(){ 
              var html = $(".copy").html();
              $(".after-add-more").after(html);
          });
          $("body").on("click",".remove",function(){ 
              $(this).parents(".control-group").remove();
          });
        });
</script>
<script>
    //Date picker
    $('#date').datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'th'
    })
</script>
<script>
    $("#keyword").select2({
                tags: true,
                tokenSeparators: [',', ' ']
            })
</script>
<script>
    $("#destination").select2({
        tags: true
    });
</script>
@endsection