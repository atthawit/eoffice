@extends('layouts.master')

@section('title')
แก้ไขหนังสือที่ถูกตีกลับ
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'แก้ไขหนังสือที่ถูกตีกลับ'])
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="{{ route('reject.update', $document->id) }}" method="POST" class="form-horizontal"
                enctype="multipart/form-data">
                {{ method_field('PATCH') }}
                @include('inbox.reject._form', ['buttonText' => "แก้ไขหนังสือที่ถูกตีกลับ"])
            </form>
        </div>
    </div>
</div>
@endsection