@extends('layouts.master')

@section('title')
แก้ไขหนังสือลงทะเบียนรับแล้ว
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'แก้ไขหนังสือลงทะเบียนรับแล้ว'])
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="{{ route('receive.update', $document->id) }}" method="POST" enctype="multipart/form-data"
                class="form-horizontal">
                {{ method_field('PATCH') }}
                @include('inbox.receive._form', ['buttonText' => "แก้ไขหนังสือลงทะเบียนรับแล้ว"])
            </form>
        </div>
    </div>
</div>
@endsection