@section('external-css')
<link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/select2/dist/css/select2.css') }}">
@endsection

@csrf
<style>

</style>
<div class="box-body">
    <div class="box-body">
        <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }}>
            <label for="name">ชื่อบทบาท</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="ชื่อบทบาท"
                value="{{ old('name', $role->name) }}" autofocus>
        </div>
        <div class="form-group" {{ $errors->has('guard_name') ? 'has-error' : '' }}>
            <label for="guard_name">guard name</label>
            <input type="text" class="form-control" id="guard_name" name="guard_name" placeholder="ชื่อ"
                value="{{ old('guard_name', $role->guard_name) }}">
        </div>
        <div class="form-group {{ $errors->has('permission') ? 'has-error' : '' }}">
            <label for="permission">สิทธิ์ของบทบาท {{ $role->name }}</label>
            <select class="form-control" multiple="multiple" id="permission" name="permission[]"
                placeholder="สิทธิ์ของบทบาท">
                @foreach ($permissions as $permission)
                <option value="{{ $permission->name }}" @if (count((array) $role->permissions) != 0)
                    @foreach ($role->permissions as $p)
                    @if ($permission->name == old('permission', $p->name))
                    selected="selected"
                    @endif
                    @endforeach
                    @endif
                    >{{ $permission->name }}</option>
                @endforeach
            </select>

            @if ($errors->has('permission'))
            <div class="help-block">
                <strong>{{ $errors->first('permission') }}</strong>
            </div>
            @endif
        </div>
    </div>
</div>
<div class="box-footer">
    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('unreceive.index') }}" class="btn btn-default btn-block">ยกเลิก</a>
        </div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary pull-right btn-block">บันทึก</button>
        </div>
    </div>
</div>

@section('external-js')
<script src="{{ asset('/AdminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
@endsection