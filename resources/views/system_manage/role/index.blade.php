@extends('layouts.master')

@section('title')
จัดการบทบาทผู้ใช้งานระบบ
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'จัดการบทบาทผู้ใช้งานระบบ'])
@endsection

@section('external-css')
<link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/select2/dist/css/select2.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                @can('create document')
                <a href="{{ route('role.create') }}" class="btn btn-default">สร้างบทบาทใหม่</a>
                @endcan
                <div class="box-tools">

                    <form action="/documents/search/result" method="GET">
                        <div class="input-group input-group-sm hidden-xs" style="width: 250px;">
                            <input type="text" name="subject" class="form-control search-box pull-right"
                                placeholder="สืบค้นบทบาทผู้ใช้งานระบบ">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px" class="text-center">#</th>
                            <th class="text-center">ชื่อบทบาท</th>
                            <th class="text-center">guard name</th>
                            <th class="text-center">สิทธิ์ของบทบาท</th>
                            <th class="text-center">วันที่เพิ่มเข้าสู่ระบบ</th>
                        </tr>
                        @if (count($roles) == 0)
                        <tr>
                            <td colspan="4" class="text-center">ไม่มีบทบาทที่สร้างไว้ในระบบ</td>
                        </tr>
                        @else
                        @foreach ($roles as $role)

                        <tr>
                            <td class="text-center m-3">{{ $loop->iteration }}</td>
                            <td class="m-3">{{ $role->name }}</td>
                            <td class="m-3">{{ $role->guard_name }}</td>
                            <td class="m-3">
                                <ol>
                                    @foreach ($role->permissions as $permission)

                                    <li>{{$permission->name}}</li>

                                    @endforeach
                                </ol>
                            </td>
                            <td class="text-center">{{ $role->created_at }}</td>

                            <td class="text-center">
                                @can('edit role')
                                <a href="{{ route('role.edit', $role->id) }}" class="btn btn-default btn-xs edit-link">
                                    <i class=" fa fa-pencil"></i>
                                </a>
                                @endcan
                                @can('delete role')
                                <a href="#" class="btn btn-default btn-xs delete-link" data-toggle="modal"
                                    data-target="#modal-delete" data-id="{{$role->id}}">
                                    <i class=" fa fa-trash"></i>
                                </a>
                                @endcan
                            </td>


                        </tr>
                        @endforeach
                        @endif

                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                {{-- <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">«</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">»</a></li>
                </ul> --}}
                <div class="pull-right">
                    {{ $roles->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('external-js')
<script src="{{ asset('/AdminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
@endsection

@section('script')
<script>
    $( document ).ready(function() {
                $(document).on("click", "", function () {
                });
        });
</script>
<script>
    $( document ).ready(function() {
        
    });
</script>
<script>
    $(document).ready(function () {
                var documents_suggestions = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: '/typeahead-search/3/?keyword=%QUERY',
                        wildcard: '%QUERY' // %QUERY will be replace by roles input in
                    }, // the url option.
                });
        
                // init Typeahead
                $('.search-box').typeahead({
                    minLength: 1,
                    highlight: true
                }, {
                    name: 'documents',
                    source: documents_suggestions,
                    display: function (item) {
                        return item;
                    },
                    limit: 5,
                    templates: {
                        suggestion: function (item) {
                            return '<div><a href="/documents/receive/' + item.id + '">' + item.subject + '</a></div>'
                        }
                    }
                });
            }); 
</script>
@endsection