@extends('layouts.master')

@section('title')
จัดการผู้ใช้งานระบบ
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'จัดการผู้ใช้งานระบบ'])
@endsection

@section('external-css')
<link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/select2/dist/css/select2.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                @can('create document')
                <a href="{{ route('user.create') }}" class="btn btn-default">สร้างผู้ใช้ใหม่</a>
                @endcan
                <div class="box-tools">

                    <form action="/documents/search/result" method="GET">
                        <div class="input-group input-group-sm hidden-xs" style="width: 250px;">
                            <input type="text" name="subject" class="form-control search-box pull-right"
                                placeholder="สืบค้นผู้ใช้งานระบบ">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px" class="text-center">#</th>
                            <th class="text-center">ชื่อ</th>
                            <th class="text-center">อีเมล</th>
                            <th class="text-center">วันที่เพิ่มเข้าสู่ระบบ</th>
                            <th class="text-center">แก้ไข / ลบ</th>
                        </tr>
                        @if (count($users) == 0)
                        <tr>
                            <td colspan="5" class="text-center">ไม่มีผู้ใช้ที่ลงทะเบียนไว้</td>
                        </tr>
                        @else
                        @foreach ($users as $user)

                        <tr>
                            <td class="text-center m-3">{{ $loop->iteration }}</td>
                            <td class="m-3">{{ $user->name }}</td>
                            <td class="m-3"><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                            <td class="text-center">{{ $user->created_at }}</td>

                            <td class="text-center">
                                @can('edit user')
                                <a href="{{ route('user.edit', $user->id) }}" class="btn btn-default btn-xs edit-link">
                                    <i class=" fa fa-pencil"></i>
                                </a>
                                @endcan
                                @can('delete user')
                                <a href="#" class="btn btn-default btn-xs delete-link" data-toggle="modal"
                                    data-target="#modal-delete" data-id="{{$user->id}}" data-fullname="{{$user->name}}">
                                    <i class=" fa fa-trash"></i>
                                </a>
                                @endcan
                            </td>


                        </tr>
                        @endforeach
                        @endif

                    </tbody>
                </table>
                @include('layouts.shared.modals._delete')
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
                {{-- <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">«</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">»</a></li>
                </ul> --}}
                <div class="pull-right">
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('external-js')
<script src="{{ asset('/AdminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
@endsection

@section('script')
<script>
    $( document ).ready(function() {
                $(document).on("click", ".delete-link", function () {
                    var user_id = $(this).data('id');
                    var user_title = $(this).data('fullname');
    
                    $('#delete-form').attr('action', '/system_manage/user/' + user_id);
                    $('.modal-title').text('ลบผู้ใช้');
                    $('#delete-modal-number-content').html('<strong>ยืนยัน</strong>การลบผู้ใช้ ' + user_title);                
            });
        });
</script>
<script>
    $( document ).ready(function() {
        
    });
</script>
<script>
    $(document).ready(function () {
                var documents_suggestions = new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: '/typeahead-search/3/?keyword=%QUERY',
                        wildcard: '%QUERY' // %QUERY will be replace by users input in
                    }, // the url option.
                });
        
                // init Typeahead
                $('.search-box').typeahead({
                    minLength: 1,
                    highlight: true
                }, {
                    name: 'documents',
                    source: documents_suggestions,
                    display: function (item) {
                        return item;
                    },
                    limit: 5,
                    templates: {
                        suggestion: function (item) {
                            return '<div><a href="/documents/receive/' + item.id + '">' + item.subject + '</a></div>'
                        }
                    }
                });
            }); 
</script>
@endsection