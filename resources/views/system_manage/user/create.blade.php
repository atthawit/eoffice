@extends('layouts.master')

@section('title')
สร้างผู้ใช้งานระบบใหม่
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'สร้างผู้ใช้งานระบบใหม่'])
@endsection

@section('external-css')
<link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/select2/dist/css/select2.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="{{ route('user.store') }}" method="POST" class="form-horizontal"
                enctype="multipart/form-data">
                @include('system_manage.user._form')
            </form>
        </div>
    </div>
</div>
@endsection

@section('external-js')
<script src="{{ asset('/AdminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
@endsection

@section('script')
<script>
    $("#role, #user_define_permission").select2({
                tags: true,
                tokenSeparators: [',', ' ']
            })
</script>

@endsection