@extends('layouts.master')

@section('title')
แก้ไขผู้ใช้ระบบ ({{ $user->name }})
@endsection

@section('header')
@include('layouts.shared._contentHeader', ['title' => 'จัดการผู้ใช้งานระบบ'])
@endsection

@section('external-css')
<link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/select2/dist/css/select2.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <form action="{{ route('user.update', $user->id) }}" method="POST" class="form-horizontal"
                enctype="multipart/form-data">
                @method('PATCH')
                {{-- {{ method_field('PATCH') }} --}}
                @include('system_manage.user._form', ['roles' => $roles])
            </form>
        </div>
    </div>
</div>
@endsection

@section('external-js')
<script src="{{ asset('/AdminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
@endsection

@section('script')
<script>
    $("#role, #user_define_permission").select2({
                tags: true,
                tokenSeparators: [',', ' ']
            })
</script>

<script>
    $(document).ready(function(){
        $("#password").keyup(function(){
            if($(this).val()){
                $("#verify_password").prop('required',true);
            }
            else $("#verify_password").prop('required',false);
        });

        $("#verify_password").keyup(function(){
            if($(this).val()){
                if ($("#password").val() != $("#verify_password").val()) {
                 $("#passwordHelpBlock").html("รหัสผ่านที่ท่านกรอกไม่ตรงกัน").css("color","red");
                 $("#save_btn").prop('disabled', true);
                }
                else{
                 $("#passwordHelpBlock").html("รหัสผ่านตรงกัน").css("color","green");
                 $("#save_btn").prop('disabled', false);
                }
            }
            else{
                $("#passwordHelpBlock").html("");
                $("#save_btn").prop('disabled', false);
            }  
      });
});
</script>

@endsection