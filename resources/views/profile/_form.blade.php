@section('external-css')
<link rel="stylesheet" href="{{ asset('/AdminLTE/bower_components/select2/dist/css/select2.css') }}">
@endsection

@csrf
<div class="box-body">
    <div>
        <ul class="nav nav-tabs" id="userTabs" role="tablist">
            <li role="presentation" class="active"><a href="#user-data" id="user-data-tab" role="tab" data-toggle="tab"
                    aria-controls="user-data" aria-expanded="true">แก้ไขข้อมูลส่วนตัว</a>
            </li>
            <li role="presentation" class=""><a href="#user-role" role="tab" id="user-role-tab" data-toggle="tab"
                    aria-controls="user-role" aria-expanded="false">บทบาทของฉัน</a></li>
            <li role="presentation" class=""><a href="#user-permission" role="tab" id="user-permission-tab"
                    data-toggle="tab" aria-controls="user-permission" aria-expanded="false">สิทธิ์ของฉัน</a>
            </li>
        </ul>
        <div class="tab-content" id="userTabContent">
            <div class="tab-pane fade active in" role="tabpanel" id="user-data" aria-labelledby="user-data-tab">
                <br />
                <img src="/uploads/avatar/{{ Auth()->user()->avatar ?? 'user-icon.png' }}"
                    class="img-circle img-responsive center-block" alt="User Image">
                <div class="box-body">
                    <div class="form-group">
                        <label for="avatar">รูปประจำตัว</label>
                        <input type="file" class="form-control" name="avatar" id="avatar">
                    </div>
                    <div class="form-group" {{ $errors->has('name') ? 'has-error' : '' }}>
                        <label for="name">ชื่อ</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="ชื่อ"
                            value="{{ old('name', $user->name) }}" autofocus>
                    </div>
                    <div class="form-group" {{ $errors->has('email') ? 'has-error' : '' }}>
                        <label for="email">อีเมล</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="อีเมล"
                            value="{{ old('email', $user->email) }}">
                    </div>
                    <div class="form-group">
                        <label for="password">รหัสผ่าน</label>
                        <input type="password" class="form-control" id="password" name="password"
                            placeholder="รหัสผ่าน">
                    </div>
                    <div class="form-group">
                        <label for="verify_password">ยืนยันรหัสผ่าน</label>
                        <input type="password" class="form-control" id="verify_password" name="verify_password"
                            placeholder="ยืนยันรหัสผ่าน">
                    </div>
                    <small id="passwordHelpBlock" class="form-text">

                    </small>
                </div>

            </div>
            <div class="tab-pane fade" role="tabpanel" id="user-role" aria-labelledby="user-role-tab">
                <div class="box-body">
                    <div class="form-group">
                        <label for="role">บทบาท</label>
                        <select class="form-control select2" multiple="multiple" id="role" name="role[]"
                            style="width: 100%;" placeholder="บทบาท">
                            @foreach ($roles as $role)
                            <option value="{{ $role->name }}" @if (count((array) $user->roles) != 0)
                                @foreach ($user->roles as $r)
                                @if ($role->name == old('role', $r->name))
                                selected="selected"
                                @endif
                                @endforeach
                                @endif
                                >{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" role="tabpanel" id="user-permission" aria-labelledby="user-permission-tab">
                <div class="box-body">
                    <strong>สิทธิ์ของผู้ใช้ปัจจุบันตามบทบาท</strong>
                    <hr />
                    @php

                    $role_with_permisstions = [];
                    $pers = [];

                    foreach ($user->roles as $role) {
                    foreach ($role->permissions as $permission) {
                    array_push($pers, $permission->name);
                    }
                    array_push($role_with_permisstions, [
                    "id" => $role->id,
                    "name" => $role->name,
                    "permission" => $pers
                    ]);
                    }
                    @endphp



                    @foreach ($role_with_permisstions as $role_with_permisstion)
                    <label for="permission{{ $role_with_permisstion['id'] }}">บทบาท :
                        {{ $role_with_permisstion['name'] }}</label>
                    <select multiple="multiple" name="permission" style="width: 100%; padding: 10px; overflow:hidden;"
                        size="{{ count($role_with_permisstion['permission']) }}">
                        @foreach ($role_with_permisstion['permission'] as $permission)
                        <option value="{{ $permission }}" disabled="disabled" style="color: #000">[{{$loop->iteration}}]
                            {{ $permission }}
                        </option>
                        @endforeach
                    </select>

                    @endforeach
                    <br>
                    <br>
                    <strong>สิทธิ์ของผู้ใช้รายบุคคลที่กำหนดเอง</strong>
                    <hr />
                    <select class="form-control select2" multiple="multiple" id="user_define_permission"
                        name="user_define_permission[]" style="width: 100%;"
                        placeholder="สิทธิ์ของผู้ใช้รายบุคคลที่กำหนดเอง">
                        @foreach ($allpermissions as $allpermission)
                        <option value="{{ $allpermission->name }}" @if (count((array) $user->getPermissionsViaRoles())!=
                            0)
                            @foreach ($user->getPermissionsViaRoles() as $gusp)
                            @if ($allpermission->name == $gusp->name)
                            disabled="disabled"
                            @endif
                            @endforeach
                            @endif

                            @if (count((array) $user->permissions)!= 0)
                            @foreach ($user->permissions as $usp)
                            @if ($allpermission->name == $usp->name)
                            selected="selected"
                            @endif
                            @endforeach
                            @endif

                            >{{ $allpermission->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>


        </div>
    </div>
</div>
<div class="box-footer">
    <div class="row">
        <div class="col-md-6">
            <a href="{{ route('unreceive.index') }}" class="btn btn-default btn-block">ยกเลิก</a>
        </div>
        <div class="col-md-6">
            <button type="submit" class="btn btn-primary pull-right btn-block">บันทึก</button>
        </div>
    </div>
</div>

@section('external-js')
<script src="{{ asset('/AdminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
@endsection