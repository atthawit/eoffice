function getAjaxErrorDetail(jqXHR, exception) {
	var msg = '';
	if (jqXHR.status === 0) {
		msg = 'Not connect.\n Verify Network.';
	} else if (jqXHR.status == 404) {
		msg = 'Requested page not found. [404]';
	} else if (jqXHR.status == 500) {
		msg = 'Internal Server Error [500].';
	} else if (exception === 'parsererror') {
		msg = 'Requested JSON parse failed.';
	} else if (exception === 'timeout') {
		msg = 'Time out error.';
	} else if (exception === 'abort') {
		msg = 'Ajax request aborted.';
	} else {
		msg = 'Uncaught Error.\n' + jqXHR.responseText;
	}

	return msg;
}

function Toast(type, css, msg) {
	this.type = type;
	this.css = css;
	this.msg = msg;
}

toastr.options = {
	"positionClass": 'toast-top-full-width',
	"closeButton": true,
	"timeOut": 5000,
	"fadeOut": 250,
	"fadeIn": 250,
}

function showToast(toasts) {
	var t = toasts;
	toastr.options.positionClass = t.css;
	toastr[t.type](t.msg);
}

$( document ).ready(function() {
    $(".ca-delete").click(function (e) {
        e.preventDefault();

        var ca_id = $(this).data('id');
        var cur_obj = $(this);

        $.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			}
        });
        
        $.ajax({
			beforeSend: function () {
				// Handle the beforeSend event
			},
			complete: function () {
				// Handle the complete event
            },
            // {{ route('ajax.delete_attachment') }}
			url: "{{ route('ajax.delete_attachment') }}",
			method: 'post',
			data: {
				'_token': '{{ csrf_token() }}',
				'ca_id': ca_id
			},
			success: function (result) {
                if (result.success) {
                    cur_obj.parent().parent(".input-group").remove();
                    showToast(new Toast('success', 'toast-top-right', 'ลบเอกสารแนบสำเร็จ'));
                }
                else if (result.error) {
                    showToast(new Toast('error', 'toast-top-right', 'ลบเอกสารแนบไม่สำเร็จ โปรดลองอีกครั้ง'));
                }
			},
			error: function (jqXHR, exception) {
                var errorDetail = getAjaxErrorDetail(jqXHR, exception);
                showToast(new Toast('error', 'toast-top-right', 'เกิดข้อผิดพลาดในการลบเอกสารแนบ' + errorDetail));
			},
		});
    });
});